package com.devx.hessian.server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Service;

import com.dexv.hessian.IDbService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Service;

import com.dexv.hessian.IDbService;

import dblogic.Category;
import dblogic.Good;
import dblogic.Unit;
import java.sql.*;

@Service("personService")
public class PersonService implements IDbService {
	
	private static Connection conn;
    private static connlogic instance;
    
    public String host = "localhost";
    public String port = "3306";    
    public String db = "catalog";
    public String login = "sa";
    public String password = "";
    
    //����������� ������ ������ � ����� ������
    public void connlogic(String host, String port, String db, String login, String password) throws Exception {
        String url = "jdbc:h2:~/"+db;
         
            try { 
               
               Class.forName("org.h2.Driver");
               conn = DriverManager.getConnection(url, login, password);
               
            } catch (ClassNotFoundException e){
                throw new Exception(e);
            } catch (SQLException e){
                throw new Exception(e);
            } catch (Exception e){
                System.out.println("e: " + e);
            }             
    }
    
    //������� ������
	public void insertGood(Good goods) throws SQLException {
        PreparedStatement stmt = null;
        
        try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
        
        try {
            
            stmt = conn.prepareStatement(
                    "INSERT INTO goods " +
                    "(id, name, cost, unit, number, category)" +
                    "VALUES (?, ?, ?, ?, ?, ?)");
            
            stmt.setInt(1, goods.get_good_id());
            stmt.setString(2, goods.get_good_name());
            stmt.setDouble(3, goods.get_good_cost());
            stmt.setInt(4, goods.get_good_unit());
            stmt.setInt(5, goods.get_good_number());
            stmt.setInt(6, goods.get_good_category());                
            stmt.execute();
            
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
	}

	//���������� ������
	public void updateGood(Good good, int prev_good_id) throws SQLException {
	        PreparedStatement stmt = null;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        try {
	            stmt = conn.prepareStatement(
	                "UPDATE goods " +
	                "SET id=?, name=?, cost=?, unit=?, number=?, category=?" +
	                "WHERE id=?");
	                
	                stmt.setInt(1, good.get_good_id());
	                stmt.setString(2, good.get_good_name());
	                stmt.setDouble(3, good.get_good_cost());
	                stmt.setInt(4, good.get_good_unit());
	                stmt.setInt(5, good.get_good_number());
	                stmt.setInt(6, good.get_good_category());
	                stmt.setInt(7, prev_good_id);
	                                    
	                stmt.execute();
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	}

	//���������� ��������� �������
	public void updateGoodsCategories(int prev_categ, int next_categ) throws SQLException {
	        PreparedStatement stmt = null;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        try {
	            stmt = conn.prepareStatement(
	                "UPDATE goods " +
	                "SET category=?" +
	                "WHERE category=?");
	                
	                stmt.setInt(1, prev_categ);
	                stmt.setInt(2, next_categ);
	                                    
	                stmt.execute();
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	}
	
	//������ ������
	public void giveGood(Good good, int amount) throws SQLException {
	        PreparedStatement stmt = null;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        try {
	            stmt = conn.prepareStatement(
	                "UPDATE goods " +
	                "SET name=?, cost=?, unit=?, number=?, category=?" +
	                "WHERE id=?");
	                                    
	                stmt.setString(1, good.get_good_name());
	                stmt.setDouble(2, good.get_good_cost());
	                stmt.setInt(3, good.get_good_unit());                    
	                stmt.setInt(4, good.get_good_number()-amount);
	                stmt.setInt(5, good.get_good_category());
	                stmt.setInt(6, good.get_good_id());
	                stmt.execute();
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	 }
	    
	//���������� ���������
	public void updateCategory(Category cat) throws SQLException {
	        PreparedStatement stmt = null;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        try {                
	        
	            stmt = conn.prepareStatement(
	                    "UPDATE categories SET category_name=?, is_deleted=? where id_category=?");
	            stmt.setString(1, cat.get_category_name());
	            stmt.setBoolean(2, cat.get_category_is_deleted());
	            stmt.setInt(3, cat.get_category_id());                
	                            
	            stmt.execute();
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	}
	 
	//���������� ������� ���������
	public void updateUnit(Unit unit) throws SQLException {
	        PreparedStatement stmt = null;           
	        int next_id;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        try {                               
	            
	            stmt = conn.prepareStatement(
	                    
	                    "UPDATE units SET name_long=?, name_short=?,  is_deleted=? WHERE id_unit=?");
	            
	            stmt.setString(1, unit.get_full_name());
	            stmt.setString(2, unit.get_short_name());                
	            stmt.setBoolean(3, unit.get_unit_is_deleted());
	            stmt.setInt(4, unit.get_unit_id());
	                            
	            stmt.execute();
	            
	        }
	        catch (Exception e){
	            System.out.println(e);
	        }
	        finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	}
	
	//������� ��. ���������
	public void insertUnit(Unit unit) throws SQLException {
	        PreparedStatement stmt = null;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        int next_id;
	        try {                
	            stmt = conn.prepareStatement("SELECT max(id_unit) FROM units");
	            ResultSet rs = stmt.executeQuery();
	            rs.beforeFirst(); 
	            rs.next(); 
	            next_id = rs.getInt(1) + 1;
	            
	            
	            stmt = conn.prepareStatement(
	                    "INSERT INTO units (id_unit, name_long, name_short) VALUES (?, ?, ?)");
	            
	            stmt.setInt(1, next_id);                
	            stmt.setString(2, unit.get_full_name());
	            stmt.setString(3, unit.get_short_name());
	            
	            stmt.execute();
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	}
	
	//���������� ��������� � �������
	public void insertCategory(Category cat) throws SQLException {
	        PreparedStatement stmt = null;
	        try {
				if (conn == null)
					connlogic(host, port, db, login, password);
			} catch (Exception e){System.out.println(e);}
			
	        int next_id;
	        try {                
	            stmt = conn.prepareStatement("SELECT max(id_category) FROM categories");
	            ResultSet rs = stmt.executeQuery();
	            rs.beforeFirst(); 
	            rs.next(); 
	            next_id = rs.getInt(1) + 1;
	            
	            
	            stmt = conn.prepareStatement(
	                    "INSERT INTO categories (id_category, id_category_parent, category_name, is_deleted) VALUES (?, ?, ?, ?)");
	
	            stmt.setInt(1, next_id);
	            stmt.setInt(2, cat.get_category_parent_id());
	            stmt.setString(3, cat.get_category_name());
	            stmt.setBoolean(4, cat.get_category_is_deleted());
	            
	            stmt.execute();
	        } finally {
	            if (stmt != null) {
	                stmt.close();
	            }
	        }
	}
		
	public Collection <Unit> getAllUnits() throws SQLException {
	    Collection <Unit> units = new ArrayList <Unit>();
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
			    	   
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
	    
	    try {
	        stmt = conn.createStatement();           
	        rs = stmt.executeQuery("SELECT id_unit, name_short, name_long, is_deleted FROM units");
	        
	        while (rs.next()){
	            Unit st = new Unit(rs);
	            units.add(st);
	        }
	        
	    } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	
	    return units;
	}
	
	public Collection <Unit> getUnits() throws SQLException {
	    Collection <Unit> groups = new ArrayList<Unit>();
	
	    Statement stmt = null;
	    ResultSet rs = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
	    
	    try {
	        stmt = conn.createStatement();
	        
	        rs = stmt.executeQuery("SELECT id_units, name_long, name_short FROM units;");
	        while (rs.next()) {
	            Unit st = new Unit(rs);
	            groups.add(st);
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    return groups;
	}
	
	//��������� �������� ��. ��������� �� ������       
	public String getCategory_Name(int id) throws SQLException {
		try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
		String result = null;
	    PreparedStatement stmt = null;
	    ResultSet rs = null;        
	    try {
	        
	        stmt = conn.prepareStatement("SELECT category_name FROM categories where id_category=?");
	        stmt.setInt(1, id);           
	        rs = stmt.executeQuery();
	        rs.beforeFirst(); 
	        rs.next();
	        result = rs.getString(1);
	        
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return result;
	}
	
	//��������� ������ ���� �������
	public Collection <Good> getAllGoods(int category) throws SQLException {
        Collection <Good> goods = new ArrayList<Good>();
        
        try {        
			if (conn == null)
				connlogic(host, port, db, login, password);
        } catch (Exception e){System.out.println(e);}
		
        
        Statement stmt = null;
        ResultSet rs = null;      
        
        try {
            stmt = conn.createStatement();
            
            if (category == -1)        
                rs = stmt.executeQuery("SELECT id, name, cost, unit, number, category FROM goods ORDER BY name, number");
            else 
                rs = stmt.executeQuery("SELECT id, name, cost, unit, number, category FROM goods WHERE category = "+category);
            
            while (rs.next()) {
                Good st = new Good(rs);                
                goods.add(st);
            }
            
        } finally {
            if (rs != null) {
                rs.close();
            }
            
            if (stmt != null) {
                stmt.close();
            }
        }

        return goods;
    }
	
	public String getUnit_Name(int id) throws SQLException {        
	    String result = null;
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	     try {
	        stmt = conn.createStatement();           
	        rs = stmt.executeQuery("SELECT name_short FROM units WHERE id_unit="+id);
	        
	        while (rs.next()){
	            result = rs.getString(1);
	        }
	        
	     } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return result;
	}
	
	public Unit getUnit(int id) throws SQLException {
	    Unit result = null;
	            
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	     try {
	        stmt = conn.createStatement();           
	        rs = stmt.executeQuery("SELECT id_unit, name_short, name_long, is_deleted FROM units WHERE id_unit=" + id);
	        
	        while (rs.next()){
	            result = new Unit(rs);
	        }
	        
	     } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	
	    return result;
	}
	
	public Unit getNthUnit(int id) throws SQLException {        
	    Unit result = null;
	            
	    Statement stmt = null;
	    ResultSet rs = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
			    
	     try {
	        stmt = conn.createStatement();           
	        rs = stmt.executeQuery("SELECT id_unit, name_short, name_long, is_deleted FROM units LIMIT " + id +","+ "1" );
	        
	        while (rs.next()){
	            result = new Unit(rs);
	        }
	        
	     } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	
	    return result;
	}
		
	public Category getCategory(int cat) throws SQLException {
	    Category result = null;
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    cat--;
	    
	     try {
	        stmt = conn.createStatement();
	                    	        
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted from categories WHERE id_category="+cat);
	        
	        while (rs.next()){
	            result = new Category(rs);
	        }
	        
	     } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return result;       
	}
	
	public Category getNthCategory(int cat) throws SQLException {
	    Category result = null;
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    //cat--;
	    
	     try {
	        stmt = conn.createStatement();
	                    
	        //rs = stmt.executeQuery("SELECT id_category, id_Category_parent, category_name, is_deleted FROM categories LIMIT " + cat +","+ "1" );
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted from categories where id_category not in (select id_category_parent from categories) LIMIT " + cat +","+ "1");
	        while (rs.next()){
	            result = new Category(rs);
	        }
	        
	     } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return result;       
	}
	
	/*public String getCategory_Name(int id) throws SQLException {
		
        try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
		String result = null;
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();            
	        rs = stmt.executeQuery("SELECT category_name FROM categories WHERE id_category="+id);
	        while (rs.next()){
	            result = rs.getString(1);
	        }
	        
	     } finally {
	        
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	
	    return result;       
	}*/
		        
	//��������� ������ ���������
	public Collection <Category> getAllCategories() throws SQLException {
	    Collection <Category> categories = new ArrayList<Category>();
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted FROM categories");
	                                              
	        while (rs.next()) {
	            Category st = new Category(rs);
	            categories.add(st);
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	
	    return categories;
	}
	
	//�������� ������
	public void removeGood(Good good) throws SQLException {
	    PreparedStatement stmt = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
	    
	    try {
	        stmt = conn.prepareStatement(
	                "DELETE FROM goods WHERE id=?");
	        stmt.setInt(1, good.get_good_id());
	        stmt.execute();
	    } finally {
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	}
	
	//�������� ������� ���������
	public void removeUnit(Unit unit) throws SQLException {
	    PreparedStatement stmt = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.prepareStatement(
	                "DELETE FROM units WHERE id_unit=?");
	        stmt.setInt(1, unit.get_unit_id());
	        stmt.execute();
	    } finally {
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	}
		
	//�������� ���������
	public void removeCategory(Category cat) throws SQLException {
	    PreparedStatement stmt = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.prepareStatement(
	                "DELETE FROM categories WHERE id_category=?");
	        stmt.setInt(1, cat.get_category_id());
	        stmt.execute();
	    } finally {
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	}
			
	//��������� ������ ���������		
	public List <Unit> getUnits(boolean filter) throws SQLException {
	    List <Unit> units = new ArrayList<Unit>();
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();            
	        rs = stmt.executeQuery("SELECT id_unit, name_long, name_short, is_deleted FROM units");
	        while (rs.next()){
	            Unit unit = new Unit();
	            unit.set_unit_id(rs.getInt(1));
	            unit.set_short_name(rs.getString(2));
	            unit.set_full_name(rs.getString(3));                
	            unit.unit_set_is_deleted(rs.getBoolean(4)); 
	            if (!filter)
	                units.add(unit);
	            else if (!unit.get_unit_is_deleted())
	                units.add(unit);
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return units;
	}
	
	//��������� �� ��������� ��������� 
	public List <Category> getCategories_f(boolean filter) throws SQLException {		
		
	    List <Category> categories = new ArrayList<Category>();
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted FROM categories");
	        while (rs.next()) {
	            Category cat = new Category();
	            
	            cat.set_category_id(rs.getInt(1));
	            cat.set_category_parent_id(rs.getInt(2));
	            cat.set_category_name(rs.getString(3));
	            cat.set_category_is_deleted(rs.getBoolean(4));
	                            
	            if (filter && !cat.get_category_is_deleted())
	                categories.add(cat);
	            if (!filter)
	            	categories.add(cat);
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return categories;
	}
	
	//��������� ������ ���������
	public List<Category> getCategories(int id_parent) throws SQLException {
	    List <Category> categories = new ArrayList<Category>();
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted FROM categories WHERE id_category_parent=" + id_parent);
	        while (rs.next()) {
	            Category cat = new Category();
	            cat.set_category_id(rs.getInt(1));
	            cat.set_category_parent_id(rs.getInt(2));
	            cat.set_category_name(rs.getString(3));
	            cat.set_category_is_deleted(rs.getBoolean(4));
	
	            categories.add(cat);
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return categories;
	}
	
	//��������� ���������-�������
	public List<Category> getCategories_leafs() throws SQLException {
	    List <Category> categories = new ArrayList<Category>();
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted from categories where (id_category not in (select id_category_parent from categories)) and is_deleted=false");
	        //rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted from categories where id_category_parent <> 0");
	        while (rs.next()) {
	            Category cat = new Category();
	            cat.set_category_id(rs.getInt(1));
	            cat.set_category_parent_id(rs.getInt(2));
	            cat.set_category_name(rs.getString(3));
	            cat.set_category_is_deleted(rs.getBoolean(4));
	            
	            categories.add(cat);
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return categories;
	}
	
	//��������� ������ ��. ���������
	public int getUnit_number(int i) throws SQLException {
	    List <Category> categories = new ArrayList<Category>();
	    int number = 0;
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();
	        rs = stmt.executeQuery("SELECT id_unit, name_long, name_short, is_deleted from units where is_deleted=false");
	        
	        while (rs.next()){
	        	Unit unit = new Unit();
	            unit.set_unit_id(rs.getInt(1));
	            unit.set_short_name(rs.getString(2));
	            unit.set_full_name(rs.getString(3));                
	            unit.unit_set_is_deleted(rs.getBoolean(4));	            	           	            
	            number++;
	            
	            if (unit.get_unit_id() == i)
	               return number;
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return 0;
	}

	public int getCategory_number(int i) throws SQLException {
	    List <Category> categories = new ArrayList<Category>();
	    int number = 0;
	    
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try {
			if (conn == null)
				connlogic(host, port, db, login, password);
		} catch (Exception e){System.out.println(e);}
		
	    try {
	        stmt = conn.createStatement();
	        rs = stmt.executeQuery("SELECT id_category, id_category_parent, category_name, is_deleted from categories where id_category not in (select id_category_parent from categories)");
	        
	        while (rs.next()) {
	            Category cat = new Category();
	            cat.set_category_id(rs.getInt(1));
	            cat.set_category_parent_id(rs.getInt(2));
	            cat.set_category_name(rs.getString(3));
	            cat.set_category_is_deleted(rs.getBoolean(4));	            
	            
	            number++;
	            
	            if (cat.get_category_id() == i)
	               return number;
	        }
	    } finally {
	        if (rs != null) {
	            rs.close();
	        }
	        
	        if (stmt != null) {
	            stmt.close();
	        }
	    }
	    
	    return 0;
	}
	
}
