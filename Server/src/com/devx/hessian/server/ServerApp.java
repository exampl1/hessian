package com.devx.hessian.server;

import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;
import org.mortbay.xml.XmlConfiguration;

public class ServerApp {
	public String host = "localhost";
    public String port = "3306";    
    public String db = "catalog";
    public String login = "sa";
    public String password = "";
    
	public static void main(String... args) {
		ServerApp app = new ServerApp();
		try {
			app.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void start() throws Exception {
		String server_config = 
				  "<Configure id=\"Server\" class=\"org.mortbay.jetty.Server\">\n"
				+ "  <Call name=\"addConnector\">\n"
				+ "    <Arg>\n"
				+ "      <New class=\"org.mortbay.jetty.nio.SelectChannelConnector\">\n"
				+ "        <Set name=\"port\"><SystemProperty name=\"jetty.port\" default=\"8084\"/></Set>\n"
				+ "      </New>\n"
				+ "    </Arg>\n"
				+ "  </Call>\n"
				+ "</Configure>\n";

		// Apply configuration to an existing object
		Server server = new Server();
		XmlConfiguration configuration = new XmlConfiguration(server_config);
		configuration.configure(server);
		
		WebAppContext webapp = new WebAppContext();
		webapp.setContextPath("/");
		webapp.setWar("web");
		server.setHandler(webapp);
		
		server.start();
	}
}
