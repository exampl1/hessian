package com.devx.hessian.server;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;

import dblogic.Category;
import dblogic.Good;
import dblogic.Unit;

/*
 * @author Andrey Pervunin
 */

/*
 *  ������ ������ � ����� ������, ��������� � �. �.
 */

public class connlogic {
    private static Connection conn;
    private static connlogic instance;
    
    //����������� ������ ������ � ����� ������
    public connlogic(String host, String port, String db, String login, String password) throws Exception {
        String url = "jdbc:h2:~/"+db;
         
            try { 
               
               Class.forName("org.h2.Driver");
               conn = DriverManager.getConnection(url, login, password);
               
            } catch (ClassNotFoundException e){
                throw new Exception(e);
            } catch (SQLException e){
                throw new Exception(e);
            } catch (Exception e){
                System.out.println("e: " + e);
            }             
     }
     
     public static synchronized connlogic getInstance(String _host, String _port, String _db, String _login, String _password) throws Exception {
        if (instance == null) {
            instance = new connlogic(_host, _port, _db, _login, _password);
        }
        return instance;
     }
}
