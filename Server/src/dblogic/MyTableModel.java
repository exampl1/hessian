package dblogic;

/*
 * @author Andrey Pervunin
 */

public interface MyTableModel {
    public boolean getHighlighted(int i);
}
