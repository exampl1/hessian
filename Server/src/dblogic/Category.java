package dblogic;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 * @author Andrey Pervunin
 */

public final class Category implements Serializable {
    
    private int category_id;
    private String category_name;
    private int category_parent_id;    
    public boolean is_deleted = false;
    
    public boolean equals(Object o){
        if (o == this) 
            return true;
        
        if (!(o instanceof Category))
            return false;
        
        Category g = (Category) o;
        
        return (g.category_id == category_id) &&
                (g.category_name == category_name) &&
                (g.is_deleted == is_deleted) &&
                (g.category_parent_id == category_parent_id);
                
    }
    
    public int hashCode(){
        int result=17;
        result = 37*result + category_id;
        result = 37*result + category_name.hashCode();
        result = 49*result + (int) 7*category_parent_id;
        
        if (is_deleted) result +=3;
        return result;
    }
    
    public Category(int _category_id, String _category_name, int _category_parent_id, boolean _is_deleted){
        super();
        category_id = _category_id;
        category_name = _category_name;
        category_parent_id = _category_parent_id;
        is_deleted = _is_deleted;
    }
    
    public Category(String cat_name){
        category_name = cat_name;
        category_id=-1;
        category_parent_id=-1;
    }
    
    public Category(){
    }
    
    public  Category (ResultSet rs) throws SQLException {

        set_category_id(rs.getInt(1));
        set_category_parent_id(rs.getInt(2));
        set_category_name(rs.getString(3));        
        set_category_is_deleted(rs.getBoolean(4));
    }
    
    public void set_category_id(int _category_id){
        this.category_id = _category_id;
    }
    
    public int get_category_id(){
        return category_id;
    }
    
    public int get_category_parent_id(){
        return category_parent_id;
    }
    
    public void set_category_name(String _new_category){
        this.category_name = _new_category;
    }
    
    public String get_category_name(){
        return category_name;
    }
    
    public void set_category_parent_id(int _new_parent_id){
        this.category_parent_id = _new_parent_id;
    }       
    
    @Override public String toString() {        
        return category_name;
    }
    
    public boolean get_category_is_deleted(){
        return is_deleted;
    }
    
    public void set_category_is_deleted(boolean _is_deleted){
        is_deleted = _is_deleted;
    }
}
