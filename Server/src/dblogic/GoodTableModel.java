package dblogic;

import java.util.Vector;
import java.util.Arrays;
import java.sql.SQLException;
import javax.swing.table.AbstractTableModel;

import com.devx.hessian.server.PersonService;

//import com.devx.hessian.server.MyTableModel;

import java.util.HashSet;


/*
 * @author Andrey Pervunin
 */
    
public class GoodTableModel extends AbstractTableModel implements MyTableModel {
    
    private Vector goods;
    private boolean Marks[];
    
    public GoodTableModel(Vector goods) {
        super();
        this.goods = goods;        
        Marks = new boolean[goods.size()];
        Arrays.fill(Marks, false);
    }

    public GoodTableModel(Vector goods, HashSet <Integer> hashSet){        
        int j;                
        this.goods = new Vector(goods.size() + 1);
               
        Marks = new boolean[goods.size() + 1];
        Arrays.fill(Marks, false);
              
        for (int i = 0; i < goods.size(); i++){                   
            if (hashSet.contains(((Good) goods.get(i)).get_good_id())){
                Marks[i] = true;       
            }
        }
        
        this.goods = goods;   
    }

    // Количество строк равно числу записей
    @Override public int getRowCount() {
        if (goods != null) {
            return goods.size();
        }
        return 0;
    }
        
    @Override public int getColumnCount() {
        return 6;
    }

    
    @Override public String getColumnName(int column) {        
        String[] colNames = {"Номер товара", "Наименование", "Цена", "Ед. измерения",  "Кол-во на складе", "Категория"};
        
        return colNames[column];
    }

    
    @Override public Object getValueAt(int rowIndex, int columnIndex) {
        if (goods != null) {
            
            Good good = (Good) goods.get(rowIndex);
           
            switch (columnIndex){
                  
                   case 0:
                    return good.get_good_id();
                   case 1:                    
                    return good.get_good_name();
                   case 2:
                    return good.get_good_cost();                    
                   case 3:
                    
                   try {
                     return connlogic.getUnit_Name(good.get_good_unit());
                   } catch (SQLException e) {
                       System.out.println(e.getMessage());
                   }
                   case 4:
                    return good.get_good_number();                    
                   case 5:
                    
                    try {
                       return  connlogic.getCategory_unit(good.get_good_category());
                    } catch (SQLException e){
                           System.out.println(e.getMessage());                    
                    }
               }            
        }
        return null;
    }
    
    public Good getGood(int rowIndex) {
        if (goods != null) {
            if (rowIndex < goods.size() && rowIndex >= 0) {
                return (Good) goods.get(rowIndex);
            }
        }
        return null;
    }
    
    @Override public boolean getHighlighted(int rowIndex){
            return Marks[rowIndex];
    }
}