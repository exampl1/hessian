package dblogic;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

import dblogic.MyTableModel;

/*
 * @author Andrey Pervunin
 */

public class CategoryTableModel extends AbstractTableModel implements MyTableModel {
    private Vector <Category> categories;
    
    public CategoryTableModel(Vector _categories){
        super();
        this.categories = _categories;
    }
     
    @Override public int getRowCount() {
        if (categories != null) {
            return categories.size();
        }
        
        return 0;
    }
    
    @Override public int getColumnCount() {
        return 3;
    }
        
    @Override public Object getValueAt(int rowIndex, int columnIndex){      

        if (categories != null){
            // �������� �� ������� ������
            Category categ = (Category) categories.get(rowIndex);          
            switch (columnIndex){
                case 0:
                    return categ.get_category_id();
                case 1:
                    return categ.get_category_name();
                case 2:
                    return categ.get_category_parent_id();
            }            
        }
        return null;
   }
    
   public Category getCategory(int rowIndex) {
        if (categories != null) {
            if (rowIndex < categories.size() && rowIndex >= 0) {
                return (Category) categories.get(rowIndex);
            }
        }
        return null;
   }
   
   @Override public boolean getHighlighted(int rowIndex){
        return false;
   }
}
