package dblogic;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 * @author Andrey Pervunin
 */

public final class Unit implements Serializable {   
    private int unit_id;
    private String unit_full_name;
    private String unit_short_name;
    public boolean is_deleted;
    
    public boolean equals(Object o){
        if (o == this) 
            return true;
        
        if (!(o instanceof Unit))
            return false;
        
        Unit u = (Unit) o;
        
        return (u.unit_id == unit_id) &&
                (u.unit_short_name == unit_short_name) &&
                (u.is_deleted == is_deleted) &&
                (u.unit_full_name == unit_full_name);        
    }
    
    public int hashCode(){
        int result=17;
        result = 37*result + unit_id;
        result = 37*result + unit_full_name.hashCode();
        result = 49*result + unit_short_name.hashCode();
        if (is_deleted) result +=3;
        return result;
    }
    
    
    public Unit() {
       
    }
    
    public Unit (ResultSet rs) throws SQLException {      
        set_unit_id(rs.getInt(1));        
        set_short_name(rs.getString(2));
        set_full_name(rs.getString(3));
        unit_set_is_deleted(rs.getBoolean(4));
    }
     
    public void set_unit_id(int _unit_id) {
        this.unit_id = _unit_id;
    }
    
    public int get_unit_id(){
        return unit_id;
    }
    
    public boolean get_unit_is_deleted(){
        return is_deleted;
    }
    
    public void set_full_name(String _new_name){
        unit_full_name = _new_name;
    }
    
    public void set_short_name(String _new_name){
        unit_short_name = _new_name;
    }    
    
    public void unit_set_is_deleted(boolean _is_deleted){
        is_deleted = _is_deleted;  
    }
    
    public boolean unit_get_is_deleted(){
        return is_deleted;  
    }
    
    public String get_short_name(){
        return unit_short_name;
    }
    
    public String get_full_name(){
        return unit_full_name;
    }
    
    @Override public String toString(){
        return unit_full_name;
    }
}
