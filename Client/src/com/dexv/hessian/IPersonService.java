package com.dexv.hessian;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import dblogic.Category;
import dblogic.Good;
import dblogic.Unit;

public interface IPersonService {
		
	public Collection <Good> getAllGoods(int category) throws SQLException;    
	public void insertGood(Good goods) throws SQLException;
    public void updateGood(Good good, int prev_good_id) throws SQLException;
    public void updateGoodsCategories(int prev_categ, int next_categ) throws SQLException;
    public void giveGood(Good good, int amount) throws SQLException;
    public void updateCategory(Category cat) throws SQLException;
    public void updateUnit(Unit unit) throws SQLException;        
    public void insertUnit(Unit unit) throws SQLException;    
    public void insertCategory(Category cat) throws SQLException;
    
    public Collection <Unit> getAllUnits() throws SQLException;
    public String getCategory_Name(int id) throws SQLException;
    public String getUnit_Name(int id) throws SQLException;
    public Category getNthCategory(int cat) throws SQLException;
    public String getCategory_unit(int id) throws SQLException;
    public String getGroup(int id) throws SQLException;
    public Collection <Category> getAllCategories() throws SQLException;
    public void removeGood(Good good) throws SQLException;
    public void removeUnit(Unit unit) throws SQLException;
    public void removeCategory(Category cat) throws SQLException;
    public void removeCategoryCascade(Category cat) throws SQLException;
    public List <Unit> getUnits(boolean filter) throws SQLException;
    public List<Category> getCategories_f(boolean filter) throws SQLException;
    public List<Category> getCategories(int id_parent) throws SQLException;
    public List<Category> getCategories_leafs() throws SQLException;
    public int getCategory_number(int i) throws SQLException;
    public Unit getUnit(int id) throws SQLException;
    public Unit getNthUnit(int id) throws SQLException;
    public Category getCategory(int cat) throws SQLException;
    
}
