package com.devx.hessian.client;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.dexv.hessian.IDbService;

import java.sql.*;

@Component("mainClient")
public class MainClient {

	public IDbService service = null;
	
	@Autowired
	public void setDbService(@Qualifier("dbService") IDbService service) {
		this.service = service;
	}
		
}
