package main;

/*
 * @author Andrey Pervunin
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Aboutdialog extends JDialog {
  public Aboutdialog(JFrame parent) {
    super(parent, "� ���������", true);
    JLabel l1, l2, l3;
    setResizable(false);
    
    Box b = Box.createVerticalBox();
    b.add(Box.createGlue());
    b.add(l1 = new JLabel("��������� "));
    b.add(l2 = new JLabel("������� �������"));
    b.add(l3 = new JLabel("(c) �������� ������"));
    b.add(Box.createGlue());
    getContentPane().add(b, "Center");
    
    l1.setAlignmentX(CENTER_ALIGNMENT);
    l2.setAlignmentX(CENTER_ALIGNMENT);
    l3.setAlignmentX(CENTER_ALIGNMENT);
    
    JPanel p2 = new JPanel();
    JButton ok = new JButton("Ok");
    p2.add(ok);
    getContentPane().add(p2, "South");

    ok.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent evt) {
        setVisible(false);
      }
    });

    setSize(250, 150);    
    setLocationRelativeTo(null);
 
  }

  public static void main() {
      
    JDialog f = new Aboutdialog(new JFrame());
    f.setVisible(true);
  }
}
