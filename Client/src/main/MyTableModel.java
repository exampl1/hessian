package main;

/*
 * @author Andrey Pervunin
 */

public interface MyTableModel {
    public boolean getHighlighted(int i);
}
