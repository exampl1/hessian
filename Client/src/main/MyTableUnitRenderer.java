package main;
import java.awt.Component;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.Color;

/*
 * @author Andrey Pervunin
 */



/* 
 * переопределим рендерер
 */

public class MyTableUnitRenderer extends DefaultTableCellRenderer {
     public MyTableUnitRenderer(){
         super();
     }
     
    @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,  
             boolean hasFocus, int row, int column) {  
         
         Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
         
         
         boolean diff_color = ((MyTableModel) table.getModel()).getHighlighted(row);
         setHorizontalAlignment(SwingConstants.CENTER);
         float a[] = new float[3];
         Color.RGBtoHSB(184, 207, 229, a);
              
         if (diff_color)
            {
                
                if (isSelected)
                    {                      
                        setBackground(Color.getHSBColor(a[0],a[1],a[2]));
                        setForeground(Color.BLACK);
                    } else {
                        setBackground(Color.YELLOW);
                        setForeground(Color.BLACK);
                    }
            } else 
            {
               
               if (isSelected)
                    {                      
                        setBackground(Color.getHSBColor(a[0],a[1],a[2]));
                        setForeground(Color.BLACK);
                        
                    } else {
                        //setBackground(Color.WHITE);
                        setForeground(Color.BLUE);
                        setForeground(Color.BLACK);
                    }

            }
         
         return this;
     } 
 }