package main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import dblogic.Unit;
import dblogic.Category;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;
import java.awt.*;


/*
 * @author Andrey Pervunin
 */


public class GiveGoodDialog extends JDialog implements ActionListener  {

    private static final int D_HEIGHT = 210;   // ������ ����
    private final static int D_WIDTH = 500;   // ������ ����
    private final static int L_X = 19;        // ����� ������� ����� ��� ����
    private final static int L_W = 135;       // ������ ����� ��� ����
    private final static int C_W = 180;       // ������ ����
    
    // �������� ������ ���� - ������ ��� ������ ������� ��� ������
    private GoodsDB owner;
    // ��������� ������� ������
    private boolean result = false;
    
    // ��������� ������� ���������
    private JTextField text_number = new JTextField();
        
    public GiveGoodDialog(GoodsDB owner) {
        super();
        this.owner = owner;        
        setResizable(false);
        
        // ���������� ���������        
        setTitle("������ ������");
        
        getContentPane().setLayout(new FlowLayout());        
        
        getContentPane().setLayout(null);
        
        // ��������� ���������� �� ���������� �����������
        JLabel label;
         
        label = new JLabel("���������� ������: ", JLabel.LEFT);
        
        label.setBounds(L_X, 20, L_W, 30);
        getContentPane().add(label);
        
        text_number.setBounds(L_X + L_W + 12, 22, C_W, 30);
        getContentPane().add(text_number);
                       
        JButton btnGive;
        
        btnGive = new JButton("������");            
        btnGive.setName("������");
                
        btnGive.addActionListener(this);
        btnGive.setBounds(L_X + L_W + C_W + 40, 10, 100, 25);
        
        Action buttonsAction = new AbstractAction(){
            
            public void actionPerformed(ActionEvent e){
                try {                   
                    GiveGoodDialog.this.actionPerformed(e);
                } catch (Exception ex){
                    System.out.println("Ex: "+ex);
                };                                         
            }
        };
        
        btnGive.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnGive.getActionMap().put("btn", buttonsAction );
                
        getContentPane().add(btnGive);
        
        JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");
        
        btnCancel.addActionListener(this);
        btnCancel.setBounds(L_X + L_W + C_W + 40, 40, 100, 25);
        
        btnCancel.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "btn");
        btnCancel.getActionMap().put("btn", buttonsAction );
                
        getContentPane().add(btnCancel);      
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        
        //�� ������
        setBounds(((int) d.getWidth() - GiveGoodDialog.D_WIDTH) / 2, ((int) d.getHeight() - GiveGoodDialog.D_HEIGHT) / 2,
                GiveGoodDialog.D_WIDTH, GiveGoodDialog.D_HEIGHT - 100);        
        
        text_number.addKeyListener(new java.awt.event.KeyListener() {
        
            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyTyped(KeyEvent e) {
                char currentChar = e.getKeyChar();
                if (currentChar != 127 && currentChar != 8 && ((currentChar < '0') || (currentChar > '9'))) {
                    e.consume();
                    JOptionPane.showMessageDialog(null, "Invalid char: '" + currentChar + "'");
                }
            }
        });
    }  
    
    //������ ������ ������    

    // ������� ������ � ���� ����� ��. ��������� � ���������������� ������    

    // �������� ���������, true - ������ ��, false - ������ Cancel
    public boolean getResult() {
        return result;
    }
    
    public int getNumber() {
        //if ()
        if (text_number.getText().length() > 0)
            return Integer.parseInt(text_number.getText());
        else return -1;
    }    
        
    @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        
        if (src.getName().equals("������")) {
            result = true;
        }
                
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
    }    
}
