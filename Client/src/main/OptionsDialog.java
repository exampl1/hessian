package main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/*
 * @author Andrey Pervunin
 */

public class OptionsDialog extends JDialog {
    
    private JTextField tfHost;
    private JTextField tfPort;
    private JTextField tfUsername;
    private JTextField tfDatabase;
    private JPasswordField pfPassword;
    
    private JLabel lbHost;
    private JLabel lbPort;
    private JLabel lbDatabase;
    private JLabel lbUsername;
    private JLabel lbPassword;
    
    private JButton btnOk;
    private JButton btnCancel;
    private boolean succeeded;
    
    public OptionsDialog(Frame parent, String Host, String Port, String Database,
                         String Username, String Password){
        super(parent, "��������� �����������", true);
        setResizable(false);
        //
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();

        cs.fill = GridBagConstraints.HORIZONTAL;

        lbHost = new JLabel("�����: ");
        cs.gridx = 0;
        cs.gridy = 11;
        cs.gridwidth = 1;
        panel.add(lbHost, cs);
        
        tfHost = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 11;
        cs.gridwidth = 1;
        panel.add(tfHost, cs);
        tfHost.setText(Host);
        
        lbPort = new JLabel("����: ");
        cs.gridx = 0;
        cs.gridy = 12;
        cs.gridwidth = 1;
        panel.add(lbPort, cs);
        
        
        tfPort = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 12;
        cs.gridwidth = 1;
        panel.add(tfPort, cs);
        tfPort.setText(Port);
        
        lbDatabase = new JLabel("���� ������: ");
        cs.gridx = 0;
        cs.gridy = 13;
        cs.gridwidth = 1;
        panel.add(lbDatabase, cs);
        
        
        tfDatabase = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 13;
        cs.gridwidth = 1;
        panel.add(tfDatabase, cs);
        tfDatabase.setText(Database);
        
        lbUsername = new JLabel("��� �����.: ");
        cs.gridx = 0;
        cs.gridy = 14;
        cs.gridwidth = 1;
        panel.add(lbUsername, cs);

        tfUsername = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 14;
        cs.gridwidth = 2;
        panel.add(tfUsername, cs);
        tfUsername.setText(Username);
        
        lbPassword = new JLabel("������: ");
        cs.gridx = 0;
        cs.gridy = 15;
        cs.gridwidth = 1;
        panel.add(lbPassword, cs);

        pfPassword = new JPasswordField(20);
        cs.gridx = 1;
        cs.gridy = 15;
        cs.gridwidth = 2;
        panel.add(pfPassword, cs);
        panel.setBorder(new LineBorder(Color.GRAY));
        pfPassword.setText(Password);
        
        Action loginAction = new AbstractAction(){            
            @Override public void actionPerformed(ActionEvent e) {
                succeeded = true;
                dispose();
            }
        };
        
        Action cancelAction = new AbstractAction(){            
            @Override public void actionPerformed(ActionEvent e) {
                succeeded = false;
                dispose();
            }
        };
        
        btnOk = new JButton("���������");
        
        btnOk.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnOk.getActionMap().put("btn", loginAction );
        btnOk.setName("��������");
                
        btnOk.addActionListener(loginAction);
        
        btnCancel = new JButton("������");
        btnCancel.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "btn");
        btnCancel.getActionMap().put("btn", loginAction );
        btnCancel.addActionListener(cancelAction);
        
        JPanel bp = new JPanel();
        bp.add(btnOk);
        bp.add(btnCancel);

        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        pack();        
        setResizable(false);
        setLocationRelativeTo(parent);
    }

    public String getHost() {
        return tfHost.getText().trim();
    }
    
    public String getPort() {
        return tfPort.getText().trim();
    }
       
    public String getUsername() {
        return tfUsername.getText().trim();
    }
    
    public String getString() {
        return tfUsername.getText().trim();
    }

    public String getDatabase() {
        return tfDatabase.getText().trim();
    }
    
    public String getPassword() {
        return new String(pfPassword.getPassword());
    }        

    public boolean isSucceeded() {
        return succeeded;
    }
}