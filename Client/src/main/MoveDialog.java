package main;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import java.util.List;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.Action;
import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import dblogic.Unit;
import dblogic.Category;
import java.sql.*;
import javax.swing.KeyStroke;

/*
 * @author Andrey Pervunin
 */

public class MoveDialog extends JDialog implements ActionListener  {

    private static final int D_HEIGHT = 210;   // ������ ����
    private final static int D_WIDTH = 500;   // ������ ����
    private final static int L_X = 19;        // ����� ������� ����� ��� ����
    private final static int L_W = 135;       // ������ ����� ��� ����
    private final static int C_W = 180;       // ������ ����
    
    // �������� ������ ���� - ������ ��� ������ ������� ��� ������
    private GoodsDB owner;
    // ��������� ������� ������
    private boolean result = false;
    
    // ��������� ������� ���������
    private JTextField text_short = new JTextField();
    private JTextField text_full = new JTextField();
     
    private ButtonGroup gender = new ButtonGroup();
    
    
    public MoveDialog(GoodsDB owner, List unitlist) {
        super();
        this.owner = owner;        
        
        // ���������� ���������        
        setTitle("����������� ������");
        setResizable(false);
        
        getContentPane().setLayout(new FlowLayout());        
        
        getContentPane().setLayout(null);
        
        // ��������� ���������� �� ���������� �����������
        
        /*text_short.setBounds(L_X + L_W + 28, 24, C_W, 30);
        getContentPane().add(text_short);*/
                       
        JButton btnMove;
        
        btnMove = new JButton("�����������");
        btnMove.setName("�����������");
                
        btnMove.addActionListener(this);
        
        Action buttonsAction = new AbstractAction(){
            
            public void actionPerformed(ActionEvent e){
                try {                   
                    MoveDialog.this.actionPerformed(e);
                } catch (Exception ex){
                    System.out.println("Ex: "+ex);
                };                                         
            }
        };
        
        btnMove.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnMove.getActionMap().put("btn", buttonsAction );
        btnMove.addActionListener(this);
        //btnMove.getActionMap()
        //btnMove.setMnemonic(KeyEvent.VK_ENTER);
        
        //play is a jButton but can be any component in the window
        InputMap im = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        btnMove.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        
        /*getRootPane().getActionMap().put("b1", new AbstractAction(){
            public void actionPerformed(ActionEvent e){ 
            
            }
        });*/
        
        //btnMove.getInputMap().put(KeyEvent.VK_1, 0), "b1");
        
        /*im.put(KeyStroke.getKeyStroke(KeyEvent.VK_1, 0), "b1");
        c.getActionMap().put("b1", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                //jButton1.doClick();
            }
        });*/
        //btnMove.getInputMap().put(KeyStroke.getKeyStroke("none"), "n1");
        
        //btnMove.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0,true),);
        /* btnMove.getActionMap().put("play", new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
          playActionPerformed(e);  // some function
        }
        });*/
        
        btnMove.setName("�����������");
        //btnGive.setBounds(L_X + L_W + C_W + 30, 5, 115, 30);
        btnMove.setBounds(L_X + L_W + C_W + 40, 10, 110, 25);
        
        getContentPane().add(btnMove);
        
        JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");
        //btnCancel.setMnemonic(KeyEvent.VK_ESCAPE);
        
        btnCancel.setBounds(L_X + L_W + C_W + 40, 40, 110, 25);
        
        btnCancel.addActionListener(this);
        
        getContentPane().add(btnCancel);
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        
        JLabel label = new JLabel("��������� ����������: ", JLabel.RIGHT);        
        try {
            
            Vector category = new Vector <Category> (GoodsDB.client.service.getCategories_f(true));
            JComboBox categ_to;            
            categ_to = new JComboBox(category);
            categ_to.setBounds(152, 19, 197, 29);
            getContentPane().add(categ_to);
            int to = ((Category) categ_to.getSelectedItem()).get_category_id();
            //int from = getSelectedRow();
            //if ()
            //if (categ_to.getSelectedItem()){
            
            
            
        } catch (SQLException excp){            
       
        }
                
        
        label.setBounds(5, 20, 142, 25);        
        
        getContentPane().add(label);
        
        
        //�� ������
        setBounds(((int) d.getWidth() - MoveDialog.D_WIDTH) / 2, ((int) d.getHeight() - MoveDialog.D_HEIGHT) / 2,
                MoveDialog.D_WIDTH, MoveDialog.D_HEIGHT - 100);        
        
        //JButton btnGive;        
    }  
        /*JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");
        btnCancel.addActionListener(this);
        btnCancel.setBounds(L_X + L_W + C_W + 40, 40, 100, 25);
        getContentPane().add(btnCancel);*/
        
       /* setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        //�� ������
        setBounds(((int) d.getWidth() - GiveGoodDialog.D_WIDTH) / 2, ((int) d.getHeight() - GiveGoodDialog.D_HEIGHT) / 2,
                GiveGoodDialog.D_WIDTH, GiveGoodDialog.D_HEIGHT - 100);
    
        @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        if (src.getName().equals("��������")) {
            result = true;
        }
        
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
        }
    }*/
    
    /*
   // public class GiveGoodDialog extends JDialog implements ActionListener  {
    //public void GiveGoodDialog(){
        
        // ���������� ���������        
     /*   setTitle("������ �����.");
        
        getContentPane().setLayout(new FlowLayout());
        
        
       
        // �� ����� ������������ layout ������
        getContentPane().setLayout(null);
        
        // ��������� ���������� �� ���������� �����������
        JLabel label;        
        label = new JLabel("���������� ������:", JLabel.LEFT);
        
        label.setBounds(L_X, 10, L_W, 30);
        getContentPane().add(label);
        text_short.setBounds(L_X + L_W + 28, 10, C_W, 20);;
        getContentPane().add(text_short);
        
        
        label.setBounds(L_X, 36, L_W, 25);
        getContentPane().add(label);        
        text_full.setBounds(L_X + L_W + 28, 36, C_W, 25);
        getContentPane().add(text_full);
        
        JButton btnGive;        
        
        btnGive = new JButton("������");
        
        btnGive.setName("������");
        btnGive.addActionListener(this);
        btnGive.setBounds(L_X + L_W + C_W + 40, 10, 100, 25);
        getContentPane().add(btnGive);
        
        /*JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");
        btnCancel.addActionListener(this);
        btnCancel.setBounds(L_X + L_W + C_W + 40, 40, 100, 25);
        getContentPane().add(btnCancel);*/
        
       /* setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        //�� ������
        setBounds(((int) d.getWidth() - GiveGoodDialog.D_WIDTH) / 2, ((int) d.getHeight() - GiveGoodDialog.D_HEIGHT) / 2,
                GiveGoodDialog.D_WIDTH, GiveGoodDialog.D_HEIGHT - 100);
    
        @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        if (src.getName().equals("��������")) {
            result = true;
        }
        
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
        }
    }*/
    
    //������ ������ ������    

    // ������� ������ � ���� ����� ��. ��������� � ���������������� ������    

    // �������� ���������, true - ������ ��, false - ������ Cancel
    public boolean getResult() {
        return result;
    }
    
    @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        
        if (src.getName().equals("�����������")) {
            result = true;
        }
        
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
    }
}
