package main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.util.Vector;
import javax.swing.KeyStroke;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.KeyEvent;
import javax.swing.JScrollPane;
import javax.swing.JOptionPane;
import java.sql.*;
import dblogic.Category;

/*
 * @author Andrey Pervunin
 */

public class CategoryDialog extends JDialog implements ActionListener  {

    private static final int D_HEIGHT = 212;   // ������ ����
    private final static int D_WIDTH = 470;   // ������ ����
    private final static int L_X = 10;      // ����� ������� ����� ��� ����
    private final static int L_W = 150;     // ������ ����� ��� ����
    private final static int C_W = 180;     // ������ ����
    // �������� ������ ���� - ������ ��� ������ ������� ��� ������
    private GoodsDB owner;
    // ��������� ������� ������
    private boolean result = false;
    // ��������� ������ (���)
    
    private JTextField categ_name = new JTextField();
    
    JComboBox categ_parent;
    
    public CategoryDialog(boolean change, GoodsDB owner) {
        
        super();
        this.owner = owner;
        setResizable(false);
        
        // ���������� ���������
        if (!change)
           setTitle("���������� ���������");
        else
           setTitle("��������� ���������");

        // �� ����� ������������ layout ������
        getContentPane().setLayout(null);
       
        JLabel label;
        
        if (!change){
            label = new JLabel("�������� ���������: ", JLabel.LEFT);
            label.setBounds(L_X, 14, L_W, 20);
        }
        else {
            label = new JLabel("����� ��������: ", JLabel.LEFT);            
            label.setBounds(L_X, 25, L_W, 20);
        }
        
        getContentPane().add(label);
        
        if (!change){
            categ_name.setBounds(L_X + L_W + 5, 14, C_W, 20);
        } else {
            categ_name.setBounds(L_W-15, 27, C_W, 20);
        }
        
        if (!change){
            try {
                
                Vector category = new Vector <Category> (GoodsDB.client.service.getCategories_f(true));
                Category newcat = new Category();
                
                newcat.set_category_id(0);
                newcat.set_category_name("������");
                newcat.set_category_parent_id(0);
                newcat.set_category_is_deleted(false);
                
                category.add(0, (Category) (newcat));
                categ_parent = new JComboBox(category);
                categ_parent.setBounds(L_X + L_W + 5, 44, C_W, 20);
                getContentPane().add(categ_parent);
                
            } catch (SQLException ex){
                
            }     
        
        }
        
        getContentPane().add(categ_name);
        
        if (!change){
            label = new JLabel("���������-��������:", JLabel.LEFT);
            label.setBounds(L_X, 44, L_W, 20);
            getContentPane().add(label);
        }
        
        JButton btnAdd;
        
        if (!change){                
            btnAdd = new JButton("��������");
        } else {
            btnAdd = new JButton("��������");            
        }
        
        Action buttonsAction = new AbstractAction(){
            
            public void actionPerformed(ActionEvent e){
                try {                   
                    CategoryDialog.this.actionPerformed(e);
                } catch (Exception ex){
                    System.out.println("Ex: "+ex);
                };                                         
            }
        };
        
        btnAdd.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnAdd.getActionMap().put("btn", buttonsAction );
                        
        getContentPane().add(btnAdd);       
               
        btnAdd.addActionListener(this);
        btnAdd.setName("��������");
        btnAdd.setBounds(L_X + L_W + C_W + 18, 10, 100, 25);
        getContentPane().add(btnAdd);
        
        btnAdd.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnAdd.getActionMap().put("btn", buttonsAction );
        btnAdd.addActionListener(this);
        
        JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");        
        
        btnCancel.addActionListener(this);
        btnCancel.setBounds(L_X + L_W + C_W + 18, 40, 100, 25);
        
        btnCancel.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "btn");
        btnCancel.getActionMap().put("btn", buttonsAction );
        btnCancel.addActionListener(this);
        
        getContentPane().add(btnCancel);                
        
        // ������������� ��������� ����� ��� �������� - �� ��������� ������.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        // �������� ������� ������
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        // � ������ ������ �������� ��� �� ������, �������� ���������� �� ������ ���������� ����������
        setBounds(((int) d.getWidth() - CategoryDialog.D_WIDTH) / 2, ((int) d.getHeight() - CategoryDialog.D_HEIGHT) / 2,
                CategoryDialog.D_WIDTH, CategoryDialog.D_HEIGHT - 100);
        
    }

    // ������� ������ � ���� ����� ��������� � ���������������� ������
    
    public Category getCategory(int new_id) {
                
        Category categ = new Category();
        
        categ.set_category_name(categ_name.getText());        
        
        categ.set_category_id(new_id);
        
        if (categ_parent == null || categ_parent.getSelectedItem() == null)
            categ.set_category_parent_id(0);
        else
            categ.set_category_parent_id( ((Category) categ_parent.getSelectedItem()).get_category_id() );
        
        return categ;
    }
        
    // �������� ���������, true - ������ ��, false - ������ Cancel
    public boolean getResult() {
        
        return result;
    }

    @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        
        if (src.getName().equals("��������")) {
            result = true;
        }
        
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
    }
}
