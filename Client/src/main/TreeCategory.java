/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.sql.*;
import dblogic.Category;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Iterator;
import javax.swing.tree.TreePath;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Color;
import java.awt.Component;

public class TreeCategory extends JPanel implements TreeSelectionListener {

private static final long serialVersionUID = 1L;
public JTree tree; // ���� ������
private JTextField jtf = new JTextField();
public JScrollPane scrpane; 

public TreePath curr_spath;

public static boolean hasEverChanged = false;


void insertBranchNodes(DefaultMutableTreeNode node, Category curr_root){
    //�������� �������� �� �������� �����
    try {
        List <Category> cat = GoodsDB.client.service.getCategories(curr_root.get_category_id());

        for (Iterator i = cat.iterator(); i.hasNext(); ){
                Category curr = (Category) i.next();
                DefaultMutableTreeNode lvlnode=new DefaultMutableTreeNode(curr);
                node.add(lvlnode);
            }
        
    } catch (SQLException e){;
        System.out.println(e);
    }
}

//fix colors
public class MyRenderer extends DefaultTreeCellRenderer {  
        Color color1 = Color.RED;  
        public MyRenderer() {  
          //leafIcon = new ImageIcon(url);  
        }  
        
 public Component getTreeCellRendererComponent(  
                            JTree tree,  
                            Object value,  
                            boolean sel,  
                            boolean expanded,  
                            boolean leaf,  
                            int row,  
                            boolean hasFocus) {  
       
            super.getTreeCellRendererComponent(  
                            tree, value, sel,  
                            expanded, leaf, row,  
                            hasFocus);
            
            DefaultMutableTreeNode tn = (DefaultMutableTreeNode) value;
                                                
            if (tn.isLeaf()){
                Category cat = (Category) tn.getUserObject();
                
                if (cat.get_category_is_deleted()){
                    setBackgroundNonSelectionColor(Color.RED);
                    setTextNonSelectionColor(Color.BLACK);
                } else {                    
                    setBackgroundNonSelectionColor(Color.WHITE);
                    setTextNonSelectionColor(Color.BLACK);                    
                }
                
            } else {
                    setBackgroundNonSelectionColor(Color.WHITE);
                    setTextNonSelectionColor(Color.BLACK);
            }
            
            return this;  
        }  
}   

public TreeCategory(){
   // ------------------------------------------
   // ���������� � ��������� ���������
   
    this.setLayout(new BorderLayout());
   
   //-------------------------------------------
   // ���������� ������		
   // �������� �����
   DefaultMutableTreeNode root = new DefaultMutableTreeNode("������");
   
   try {
        //�������� �������� �� �������� �����
        //List <Category> cat = GoodsDB.dbconnection.getCategories(0);
	   	List <Category> cat = GoodsDB.client.service.getCategories(0);
        
	   	for (Iterator i = cat.iterator(); i.hasNext(); ){
            Category curr = (Category) i.next();
            DefaultMutableTreeNode lvlnode=new DefaultMutableTreeNode(curr);
            root.add(lvlnode);
            insertBranchNodes(lvlnode, curr);
        }        
   } catch (SQLException e){;
        System.out.println(e);
   }
            
   // ������� ������� ���������� � ��������� ������� ������
   tree = new JTree(root);
   MyRenderer my_renderer = new MyRenderer();
   //tree.setCellRenderer(my_renderer);
        
   tree.addTreeSelectionListener(this);
   
   for (int i = 0; i < tree.getRowCount(); i++) {
         tree.expandRow(i);
   }
   // ��������� �������� ���������� � ����
   
   scrpane = new JScrollPane(tree);
   this.add(scrpane);
   
   setVisible(true); // ���������� ����
}

public void refreshTreeCategory(){
    //TreeCategory();
    // ------------------------------------------
   // ���������� � ��������� ���������
   //Container c = getContentPane(); // ���������� ������� ����
   //c.setLayout(new BorderLayout()); // �������� �����������
   this.setLayout(new BorderLayout());
   //-------------------------------------------
   // ���������� ������		
   // �������� �����
   DefaultMutableTreeNode root = new DefaultMutableTreeNode("������");
   
   try {
        //�������� �������� �� �������� �����
        List <Category> cat = GoodsDB.client.service.getCategories(0);
         
        for (Iterator i = cat.iterator(); i.hasNext(); ){
            Category curr = (Category) i.next();
            DefaultMutableTreeNode lvlnode=new DefaultMutableTreeNode(curr);
            root.add(lvlnode);
            insertBranchNodes(lvlnode, curr);
        }        
   } catch (SQLException e){;
        System.out.println(e);
   }
      
   // ������� ������� ���������� � ��������� ������� ������
   //tree = new JTree(root);
   //tree.addTreeSelectionListener(this);
   
   MyRenderer my_renderer = new MyRenderer();
   tree.setCellRenderer(my_renderer);
   
   for (int i = 0; i < tree.getRowCount(); i++) {
         tree.expandRow(i);
   }
   // ��������� �������� ���������� � ����
   
   scrpane = new JScrollPane(tree);
   this.add(scrpane); 
   
   setVisible(true); // ���������� ����
}

// ����� ���������� TreeSelectionListener
    public void valueChanged(TreeSelectionEvent arg0) {
      hasEverChanged = true;      
      curr_spath = arg0.getNewLeadSelectionPath();  
      
    }
}