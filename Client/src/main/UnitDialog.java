package main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import dblogic.Unit;
import dblogic.Category;
import java.awt.event.KeyAdapter;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.lang.reflect.Method;

/*
 * @author Andrey Pervunin
 */

public class UnitDialog extends JDialog implements ActionListener {

    private static final int D_HEIGHT = 210;   // ������ ����
    private final static int D_WIDTH = 500;   // ������ ����
    private final static int L_X = 19;        // ����� ������� ����� ��� ����
    private final static int L_W = 135;       // ������ ����� ��� ����
    private final static int C_W = 180;       // ������ ����
    
    // �������� ������ ���� - ������ ��� ������ ������� ��� ������
    private GoodsDB owner;
    // ��������� ������� ������
    private boolean result = false;
    
    // ��������� ������� ���������
    private JTextField text_short = new JTextField();
    private JTextField text_full = new JTextField();
     
    private ButtonGroup gender = new ButtonGroup();
    
    public int a= 123;
     
    // ������ �������� - ���������� ��. ��������� ��� �����������
    public UnitDialog(boolean change, GoodsDB owner) {
        super();
        this.owner = owner;
        
        setResizable(false);
        //try {
        // ���������� ���������
        if (!change)
            setTitle("���������� ����� ������� ���������");
        else 
            setTitle("�������������� ������� ���������");
        
        getContentPane().setLayout(new FlowLayout());
        
        // �� ����� ������������ layout ������
        getContentPane().setLayout(null);
        
        // ��������� ���������� �� ���������� �����������
        JLabel label;
        
        if (!change)
            label = new JLabel("����������� ��������:", JLabel.LEFT);
        else 
            label = new JLabel("����� ����. ��������:", JLabel.LEFT);
        
        label.setBounds(L_X-10, 10, L_W+10, 30);
        getContentPane().add(label);
        text_short.setBounds(L_X + L_W + 28, 15, C_W, 20);;
        getContentPane().add(text_short);
        
        //������ ��������
        if (!change)
            label = new JLabel("������ �������� ��.:", JLabel.LEFT);
        else 
            label = new JLabel("����� ������ ����-�:", JLabel.LEFT);
        
        label.setBounds(L_X-10, 36, L_W + 13, 25);
        getContentPane().add(label);        
        
        text_full.setBounds(L_X + L_W + 28, 39, C_W, 20);
        getContentPane().add(text_full);
        
        final JButton btnAdd;
        
        if (!change){
            btnAdd = new JButton("��������");            
        } else {
            btnAdd = new JButton("��������");           
        }
        
        try {                
            Action buttonsAction = new AbstractAction(){
            
            public void actionPerformed(ActionEvent e){
                try {                   
                    UnitDialog.this.actionPerformed(e);
                } catch (Exception ex){
                    System.out.println("Ex: "+ex);
                };                
            }
        };
        
        btnAdd.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnAdd.getActionMap().put("btn", buttonsAction );
                                        
        btnAdd.setName("��������");
        btnAdd.addActionListener(this);
        btnAdd.setBounds(L_X + L_W + C_W + 40, 10, 100, 25);
        getContentPane().add(btnAdd);
        
        JButton btnCancel = new JButton("�����");
        
        btnCancel.setMnemonic(KeyEvent.VK_ESCAPE);        
        btnCancel.setName("�����");
        
        btnCancel.addActionListener(this);
        btnCancel.setBounds(L_X + L_W + C_W + 40, 40, 100, 25);
        btnCancel.addActionListener(this);
        
        btnCancel.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "btn");
        btnCancel.getActionMap().put("btn", buttonsAction );
                
        getContentPane().add(btnCancel);      
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        //�� ������
        setBounds(((int) d.getWidth() - UnitDialog.D_WIDTH) / 2, ((int) d.getHeight() - UnitDialog.D_HEIGHT) / 2,
                UnitDialog.D_WIDTH, UnitDialog.D_HEIGHT - 100);
       
       } catch (Exception ex){
           System.out.println(ex);
       };
    }
                   
    // ������� ������ � ���� ����� ��. ��������� � ���������������� ������
    public Unit getUnit() {
        Unit unit = new Unit();
        
        //unit.set_unit_id();        
        unit.set_full_name(text_full.getText());
        unit.set_short_name(text_short.getText());
        unit.unit_set_is_deleted(false);
        //unit.
        return unit;
    }
    
    // �������� ���������, true - ������ ��, false - ������ Cancel
    public boolean getResult() {
        return result;
    }
    
    @Override public void actionPerformed(ActionEvent e) {
        
         JButton src = (JButton) e.getSource();
        
        if (src.getName().equals("��������")) {
            result = true;
        }
        
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
    }    
}
