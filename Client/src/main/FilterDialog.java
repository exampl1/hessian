package main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
        
import dblogic.Unit;
import dblogic.Category;

/*
 * @author Andrey Pervunin
 */

public class FilterDialog extends JDialog implements ActionListener  {

    private static final int D_HEIGHT = 210;   // ������ ����
    private final static int D_WIDTH = 500;   // ������ ����
    private final static int L_X = 19;        // ����� ������� ����� ��� ����
    private final static int L_W = 135;       // ������ ����� ��� ����
    private final static int C_W = 180;       // ������ ����
    
    // �������� ������ ���� - ������ ��� ������ ������� ��� ������
    private GoodsDB owner;
    
    // ��������� ������� ������
    private boolean result = false;
    
    // ��������� ������� ���������
    private JTextField text_short = new JTextField();
    private JTextField text_full = new JTextField();
    
    JComboBox categList;
    //private ButtonGroup gender = new ButtonGroup();
    
    
    public FilterDialog(GoodsDB owner, List <Category> categories) {
        super();
        this.owner = owner;        
        setResizable(false);
        
        // ���������� ���������        
        setTitle("���������� �� ��������� ������");
        
        getContentPane().setLayout(new FlowLayout());        
        
        getContentPane().setLayout(null);
        
        // ��������� ���������� �� ���������� �����������
                             
        JButton btnGive;
        
        Action buttonsAction = new AbstractAction(){
            
            public void actionPerformed(ActionEvent e){
                try {                   
                    FilterDialog.this.actionPerformed(e);
                } catch (Exception ex){
                    System.out.println("Ex: "+ex);
                };
            }
        };
        
        btnGive = new JButton("������");
        btnGive.setName("������");
        btnGive.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnGive.getActionMap().put("btn", buttonsAction );
               
        btnGive.addActionListener(this);
        btnGive.setBounds(L_X + L_W + C_W + 40, 10, 100, 25);
        
        getContentPane().add(btnGive);
        
        JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");
        btnCancel.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "btn");
        btnCancel.getActionMap().put("btn", buttonsAction );
                
        btnCancel.addActionListener(this);
        btnCancel.setBounds(L_X + L_W + C_W + 40, 40, 100, 25);
        getContentPane().add(btnCancel);      
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        
        JLabel label = new JLabel("���������: ", JLabel.RIGHT);        
        
        Category cat = new Category("(��� ���������)");
                
        categories.add(0, cat);
        categList = new JComboBox (new Vector <Category> (categories));
                
        label.setBounds(10, 20, 100, 25);
        categList.setBounds(120, 20, 200, 25);
        
        getContentPane().add(label);
        getContentPane().add(categList);
        
        //�� ������
        setBounds(((int) d.getWidth() - FilterDialog.D_WIDTH) / 2, ((int) d.getHeight() - FilterDialog.D_HEIGHT) / 2,
                FilterDialog.D_WIDTH, FilterDialog.D_HEIGHT - 100);                
    }
    
    //������ ������ ������    

    // ������� ������ � ���� ����� ��. ��������� � ���������������� ������    

    // �������� ���������, true - ������ ��, false - ������ Cancel
    public boolean getResult() {
        return result;
    }
    
    public int getCategory() {
       return ((Category) categList.getSelectedItem()).get_category_id();
    }
    
    @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        
        if (src.getName().equals("������")) {
            result = true;
        }
        
        if (src.getName().equals("�����")) {
            result = false;
        }
        
        setVisible(false);
    }    
}
