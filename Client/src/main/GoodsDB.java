package main;

/*
 * @author pc
 */

import dblogic.CategoryTableModel;
import dblogic.UnitTableModel;
import dblogic.GoodTableModel;

import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.Vector;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.HashSet;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.tree.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import dblogic.Good;
import dblogic.Unit;
import dblogic.Category;
import dblogic.connlogic;

import java.awt.Color;

import com.devx.hessian.client.MainClient;

public class GoodsDB extends JFrame implements ActionListener {
    
    private static final String INSERT_GD = "insertGood";
    private static final String CHANGE_GD = "changeGood";
    private static final String GIVE_GD   = "giveGood";
    private static final String DELETE_GD = "deleteGood";
    private static final String FILTER_GD = "filterGood";
    
    private static final String INSERT_UT = "insertUnit";
    private static final String CHANGE_UT = "changeUnit";
    private static final String DELETE_UT = "deleteUnit";
    
    private static final String INSERT_CAT = "insertCategory";
    private static final String CHANGE_CAT = "changeCategory";
    private static final String DELETE_CAT = "deleteCategory";        
    private static final String MOVE_CAT = "moveCategory";
    
    private static final String EXIT = "Exit";
    private static final String RECONNECT = "Reconn";
    private static final String OPTIONS = "Options";
    private static final String ABOUT = "About";
    private static final String HELP = "Help";
    
    private static final String REMOVE_H = "REMOVE_H";
    
    static int i = 0, d_width;
                   //������, ������� ���������, ���������
    private JTable goodsList, unitList, catList;
    
    ListSelectionModel lm1 = new DefaultListSelectionModel(),
                       lm2 = new DefaultListSelectionModel();
                    
    static ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
    public static final MainClient client = (MainClient) context.getBean("mainClient");
    
    HashSet <Integer> hSet = null;        
    
    JMenuItem menuExit, reconnect,
              connOpts, help, about;
    
    JMenu f_menu, o_menu, h_menu;
    
    public String host = "localhost";
    public String port = "3306";
    
    public String db = "catalog";
    public String login = "sa";
    public String password = "";
    
    public TreeCategory tc;
    
    JPanel Goods, Measure_units, Categories;

    
//�����������
public GoodsDB() throws Exception {
            
            super("������� �������");
            
            
            JMenuBar menuBar = new JMenuBar();
            f_menu = new JMenu("����");
            o_menu = new JMenu("���������");                
            h_menu = new JMenu("������");
            final JFrame f = this;                
            
            menuBar.add(f_menu);
            menuBar.add(o_menu);               
            menuBar.add(Box.createHorizontalGlue());
            menuBar.add(h_menu);
            
            reconnect = new JMenuItem("���������������");
            reconnect.setName(RECONNECT);
            reconnect.addActionListener(this);
            
            menuExit = new JMenuItem("�����");
            
            connOpts = new JMenuItem("����� �����������");
            help = new JMenuItem("�������");
            help.setName(HELP);
            h_menu.add(help);
            help.addActionListener(this);
            
            about = new JMenuItem("� ���������");
            about.setName(ABOUT);
            about.addActionListener(this);                
            
            f_menu.add(reconnect);
            f_menu.add(menuExit);                
            o_menu.add(connOpts);
            
            h_menu.add(help);
            h_menu.add(about);
            
            menuExit.setName(EXIT);
            menuExit.addActionListener(this);
            
            connOpts.setName(OPTIONS);
            connOpts.addActionListener(this);                
            
            setJMenuBar(menuBar);
            
            getContentPane().setLayout(new BorderLayout());
            
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            final JTabbedPane tabbedPane = new JTabbedPane();

            JPanel content = new JPanel();
			content.setLayout(new BorderLayout());
            
			tabbedPane.addTab("�������", Goods = new JPanel());
            tabbedPane.addTab("������� ���������", Measure_units = new JPanel());
            tabbedPane.addTab("��������� �������", Categories = new JPanel());                
                        
            content.add(tabbedPane, BorderLayout.CENTER);
            
            Goods.setLayout(new BorderLayout());
            Goods.add(new JLabel("������:"), BorderLayout.NORTH);
            
            lm1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            lm2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            
            goodsList = new JTable(1, 10);
            unitList = new JTable(1, 2);
            catList = new JTable(1, 10);                
                            
            Goods.add(new JScrollPane(goodsList), BorderLayout.CENTER);
            
            goodsList.setModel(new GoodTableModel(new Vector <Good> (client.service.getAllGoods(-1))));
                                            
            unitList.setModel(new UnitTableModel(new Vector <Unit> (client.service.getAllUnits())));
                           
            JButton btnAddGood = new JButton("��������");
            
            btnAddGood.setName(INSERT_GD);               
            btnAddGood.addActionListener(this);
            
            JButton btnUpdGood = new JButton("��������");                
            btnUpdGood.setName(CHANGE_GD);
            btnUpdGood.addActionListener(this);
            
            JButton giveGood = new JButton("������ �����");
            giveGood.setName(GIVE_GD);
            giveGood.addActionListener(this);
            
            JButton btnDelGood = new JButton("�������");                
            btnDelGood.setName(DELETE_GD);
            btnDelGood.addActionListener(this);
            
            JButton btnfilterGood = new JButton("����������");
            btnfilterGood.setName(FILTER_GD);
            btnfilterGood.addActionListener(this);
            
            // ������� ������, �� ������� ������� ���� ������ � ������ �� ����
            JPanel pnlBtnSt = new JPanel();
            pnlBtnSt.setLayout(new GridLayout(1, 5));
            
            pnlBtnSt.add(btnAddGood);
            pnlBtnSt.add(btnUpdGood);
            pnlBtnSt.add(giveGood);
            pnlBtnSt.add(btnDelGood);
            pnlBtnSt.add(btnfilterGood);                
            
            Goods.add(pnlBtnSt, BorderLayout.SOUTH);               
            
            Measure_units.setLayout(new BorderLayout());
            Measure_units.add(new JLabel("�������:"), BorderLayout.NORTH);
            Measure_units.add(new JScrollPane(unitList), BorderLayout.CENTER);
            
            JButton btnAddGr = new JButton("��������");
            btnAddGr.setName(INSERT_UT);
            btnAddGr.addActionListener(this);
            
            JButton btnUpdGr = new JButton("��������");                
            btnUpdGr.setName(CHANGE_UT);
            btnUpdGr.addActionListener(this);
            
            JButton btnDelGr = new JButton("�������");                
            btnDelGr.setName(DELETE_UT);
            btnDelGr.addActionListener(this);
            
            // ������� ������, �� ������� ������� ���� ������ � ������ �� ����
            JPanel pnlBtnGr = new JPanel();                
            pnlBtnGr.setLayout(new GridLayout(1, 3));
            pnlBtnGr.add(btnAddGr);
            pnlBtnGr.add(btnUpdGr);
            pnlBtnGr.add(btnDelGr);
            Measure_units.add(pnlBtnGr, BorderLayout.SOUTH);
            
            Categories.setLayout(new BorderLayout());
            Categories.add(new JLabel("���������:"), BorderLayout.NORTH);
            
            
            
            tc = new TreeCategory();
            MyRenderer my_renderer = new MyRenderer();                
      
            tc.setSize(600, 400);
            Categories.add(tc);
                                            
            JButton btnAddCat = new JButton("��������");
            btnAddCat.setName(INSERT_CAT);             
            btnAddCat.addActionListener(this);
            
            JButton btnUpdCat = new JButton("��������");               
            btnUpdCat.setName(CHANGE_CAT);
            btnUpdCat.addActionListener(this);
            
            JButton btnDelCat = new JButton("�������");                
            btnDelCat.setName(DELETE_CAT);
            btnDelCat.addActionListener(this);
            
            JButton btnMovCat = new JButton("����������� ������");                
            btnMovCat.setName(MOVE_CAT);
            btnMovCat.addActionListener(this);
            
            JPanel pnlBtnCat = new JPanel();                
            pnlBtnCat.setLayout(new GridLayout(1, 4));
            
            pnlBtnCat.add(btnAddCat);
            pnlBtnCat.add(btnUpdCat);
            pnlBtnCat.add(btnDelCat);
            pnlBtnCat.add(btnMovCat);
            
            Categories.add(pnlBtnCat, BorderLayout.SOUTH);                                
            
            setLocationRelativeTo(null);
            setVisible(true);
            pack();
            
            DefaultTableCellRenderer jTableRenderer = new DefaultTableCellRenderer() {
                @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                
                try {                        
                    Unit u = client.service.getNthUnit(row);        
                    
                    if (u.get_unit_is_deleted())                            
                        cell.setBackground(Color.RED);
                    else 
                        cell.setBackground(Color.WHITE);
                    
                    if (isSelected){
                        setBackground(new Color(184, 207, 229));                            
                    }
                    
                } catch (SQLException e){
                    System.out.println(e);
                }
                                    
              return cell;
             }
            };             
            
            unitList.setDefaultRenderer(Object.class, jTableRenderer);
                                                                                           
            goodsList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);                
            unitList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            
            getContentPane().add(content);
            d_width = setColumnsWidth(goodsList);
            setBounds(100, 100, 80+d_width, 500);
            
            setLocationRelativeTo(null);
}

//������������ ������� � �������   
public void reloadGoods(){
     try {
                
                       goodsList.setModel(new GoodTableModel(new Vector<Good>(client.service.getAllGoods(-1))));
                
                       d_width = setColumnsWidth(goodsList);

          } catch (SQLException e) {
                   JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
          }
}    

//������������ �������
public void reloadGoods(final HashSet _hset) {
      try {
                    //if (client.service.get != null)
                        goodsList.setModel(new GoodTableModel(new Vector<Good>(client.service.getAllGoods(-1)), _hset));
                    //else
                    //    goodsList.setModel(new GoodTableModel(new Vector<Good>()));
                    
                    d_width = setColumnsWidth(goodsList);
                    } catch (SQLException e) {
                        JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
                    }
}

//������������ ����� � �������
public void reloadUnits() {
    // ������� ��������� ����� ��� ������
    Thread inner = new Thread() {
        // �������������� � ��� ����� run
        @Override public void run() {
            if (unitList != null) {
               try {
                   //if (client.service != null)
                     unitList.setModel(new UnitTableModel(new Vector<Unit>(client.service.getAllUnits())));                       
                   //else 
                     //unitList.setModel(new UnitTableModel(new Vector<Unit>()));
               } catch (SQLException e) {
                        JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
               }
            }
            
            DefaultTableCellRenderer jTableRenderer = new DefaultTableCellRenderer() {
                @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                
                try {
                    
                    Unit u = client.service.getNthUnit(row);
                    
                    if (u.get_unit_is_deleted())
                        cell.setBackground(Color.RED);
                    else 
                        cell.setBackground(Color.WHITE);
                    
                } catch (SQLException e){
                    System.out.println(e);
                }
                                    
              return cell;
             }
            };
        }            
        // ��������� ������ ������ run
    };
            
    // ��������� ����������� ���������� ������
    // � ������ �� ��������� ��� �����
    inner.start();
}
   
// ������� ������� ������ �������
public static int setColumnsWidth(JTable table) {
     int sum = 0;
   
     JTableHeader theta = table.getTableHeader();  
     for (int j = 0; j < table.getColumnCount(); j++) {  
         TableColumn column = table.getColumnModel().getColumn(j);  
        int prefWidth =   Math.round((float) theta.getFontMetrics(  
                    theta.getFont()).getStringBounds(theta.getTable().getColumnName(j),  
                     theta.getGraphics()  
                 ).getWidth()  
             );           
         if ((j != 0) & (j != 3)){
             column.setPreferredWidth(prefWidth + 20);
             sum += prefWidth + 20;
         } else {
             column.setPreferredWidth(prefWidth + 4);
             sum += prefWidth + 4;
         }
     }

     return sum;
} 

//
public static void main(String[] args){               
	javax.swing.SwingUtilities.invokeLater(new Runnable() {
        @Override public void run() {                           
                  try {                	  
                	  new GoodsDB();                	  
                  }  catch (Exception ex){
                      System.out.println(ex.toString());
                  }
	}
    });            
}

//���������� ������ ������
private void insertGood() {
    
            try {                    
                final List <Unit> unitList;
                unitList = client.service.getUnits(true);
                
                if (unitList.size() == 0){
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� ������� ������� ��. ���������.");
                    return;
                }
                
                final List <Category> catList;
                catList = client.service.getCategories_f(true);
                
                if (catList.size() == 0){
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� ������� ������� ���������.");
                    return;
                }
                                    
                GoodDialog sd = new GoodDialog(client.service.getUnits(true), catList, false, GoodsDB.this, null);
                sd.setModal(true);
                sd.setVisible(true);
                
                if (sd.getResult()) {
                    Good s = sd.getGood();
                    client.service.insertGood(s);
                    reloadGoods();
                }
                
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
            }
}

private void deleteGood() {
            if (goodsList != null) {
                GoodTableModel stm = (GoodTableModel) goodsList.getModel();
                // ��������� - ������� �� ���� �����-������ �����
                if (goodsList.getSelectedRow() >= 0) {
                            
                            JOptionPane optPane= new JOptionPane();                                   
                            UIManager.put("OptionPane.yesButtonText", "��");
                            UIManager.put("OptionPane.noButtonText", "���");
                            optPane.updateUI();
                            
                    if (optPane.showConfirmDialog(GoodsDB.this,
                            "�� ������������� ������ ������� ������ �����?", "�������� ������",
                            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                           
                           
                        Good s = stm.getGood(goodsList.getSelectedRow());
                        
                        try {
                        	client.service.removeGood(s);                                
                        } catch (SQLException e){
                            JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
                        }
                        
                        reloadGoods();
                    }
                } // ���� ����� �� ������� - �������� ������������, ��� ��� ����������
                else {
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ����� � �������");
                }
            }            
}

//��������� ������
private void changeGood(){
            try {
            if (goodsList != null) {
                GoodTableModel stm = (GoodTableModel) goodsList.getModel();
                // ������� �� �����
                if (goodsList.getSelectedRow() >= 0){
                        Good g = stm.getGood(goodsList.getSelectedRow());
                        GoodDialog sd = new GoodDialog(client.service.getUnits(true), client.service.getCategories_f(true), true, GoodsDB.this, g);
                        
                        //������������� ������ ����������� ������
                        
                        sd.setModal(true);
                        sd.setVisible(true);
                        
                        if (sd.getResult()) {
                            Good s = sd.getGood();                                
                            client.service.updateGood(s, g.get_good_id());
                            sd.setModal(true);
                            sd.setVisible(true);                                                    
                            
                            reloadGoods();
                        }
                        
                } //���������� �������� ����� � ������
                else {
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ����� � �������.");
                }
              
            }
            
            } catch (SQLException e){
                JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
            }
}    

//���������� ������� ���������
private void insertUnit(){
            try {                    
                
                List <Category> f = client.service.getCategories_f(true);
                
                if (f.size() == 0){
                         JOptionPane.showMessageDialog(GoodsDB.this, "���������� ������� ������� ���������.");
                         return;
                }
                
                UnitDialog sd = new UnitDialog(false, GoodsDB.this);
                
                sd.setModal(true);
                sd.setVisible(true);
                
                if (sd.getResult()){
                    Unit c = sd.getUnit();
                    client.service.insertUnit(c);
                    reloadUnits();
                }
                
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
            }
        }

//��������� ������� ���������
private void changeUnit() {
    
             if (unitList != null) {
                UnitTableModel gtm = (UnitTableModel) unitList.getModel();
                
                if (unitList.getSelectedRow() >= 0) {
                    Unit g = gtm.getUnit(unitList.getSelectedRow());                        
                    
                    if (g.get_unit_is_deleted()){
                       JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��������� ������� ���������.");
                       return;
                    }
                    
                    try {
                        UnitDialog gd = new UnitDialog(true, GoodsDB.this);
                          gd.setModal(true);
                          gd.setVisible(true);
                          if (gd.getResult()){
                              Unit g_new = gd.getUnit();
                              g_new.set_unit_id(g.get_unit_id());
                              client.service.updateUnit(g_new);
                              reloadUnits();
                          }                        
                        } catch (Exception e) {
                        JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
                    }
                } 
                else {
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��. ��������� � �������");
                }
            }
}


//�������� ������� ���������
private void deleteUnit(){ 
            if (unitList != null) {
                UnitTableModel gtm = (UnitTableModel) unitList.getModel();
                
                if (unitList.getSelectedRow() >= 0) {
                    Unit unit = gtm.getUnit(unitList.getSelectedRow());
                    if (unit.get_unit_is_deleted()){
                        if (JOptionPane.showConfirmDialog(GoodsDB.this,
                            "�������� �������� ��. ���������?", "�������� ��. ���������",
                            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                                unit.unit_set_is_deleted(false);
                              try {
                            	  client.service.updateUnit(unit);
                                reloadUnits();
                              } catch (SQLException e){}
                            }
                        }
                    
                    else 
                    if (JOptionPane.showConfirmDialog(GoodsDB.this,
                            "�� ������������� ������ ������� ������ ������� ���������?", "�������� ��. ���-�",
                            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        
                        
                        
                        Unit g = gtm.getUnit(unitList.getSelectedRow());
                        try {
                            g.unit_set_is_deleted(true);
                            client.service.updateUnit(g);
                            
                            reloadUnits();
                        } catch (SQLException e) {
                            JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());                                
                        }                           
                    }
                } 
            }
            else {
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��. ��������� � �������");
           }
}            

public class MyRenderer extends DefaultTreeCellRenderer {  
    Color color1 = Color.RED;  
    public MyRenderer() {  
        
    }  
    
public Component getTreeCellRendererComponent(  
                        JTree tree,  
                        Object value,  
                        boolean sel,  
                        boolean expanded,  
                        boolean leaf,  
                        int row,  
                        boolean hasFocus) {  
   
        super.getTreeCellRendererComponent(  
                        tree, value, sel,  
                        expanded, leaf, row,  
                        hasFocus);
        
        DefaultMutableTreeNode tn = (DefaultMutableTreeNode) value;
                                            
        if ((tn.isLeaf() && !tn.isRoot()) || ((tn.getParent() != null) && !(tn.getParent().isLeaf())) ){
            Category cat = (Category) tn.getUserObject();
            
            if (cat.get_category_is_deleted()){
                setBackgroundNonSelectionColor(Color.RED);
                setTextNonSelectionColor(Color.BLACK);
            } else {                    
                setBackgroundNonSelectionColor(Color.WHITE);
                setTextNonSelectionColor(Color.BLACK);                    
            }
            
        } else {
                setBackgroundNonSelectionColor(Color.WHITE);
                setTextNonSelectionColor(Color.BLACK);
        }
        
        return this;  
    }  
}   

public void refreshCat(){
      
      Categories.remove(tc);                        
      Categories.revalidate();
      Categories.repaint();
                    
      tc = new TreeCategory();
      MyRenderer my_renderer = new MyRenderer();
      tc.tree.setCellRenderer(my_renderer);
                                    
      tc.setSize(600, 400);
                    
      Categories.add(tc);                        
      Categories.revalidate();
      Categories.repaint();
}

//������� ���������
private void insertCategory() {
    Thread t = new Thread() {

        @Override public void run() {
           
                // ��������� ����� ��������� - false
                CategoryDialog sd = new CategoryDialog(false, GoodsDB.this);
                sd.setModal(true);
                sd.setVisible(true);
                
                if (sd.getResult()){
                    Category cat = sd.getCategory(0);                        
                    try {
                    	client.service.insertCategory(cat);
                    } catch (SQLException ex){
                        System.out.println(ex);
                    }
                    refreshCat();
                }                                   
        }
    };
    t.start();
}

//��������� ���������
private void changeCategory(){
    Thread t = new Thread(){
        
        
        @Override public void run(){                
              if (TreeCategory.hasEverChanged){
                try {                        
                    CategoryDialog sd = new CategoryDialog(true, GoodsDB.this);
                    
                    DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tc.curr_spath.getLastPathComponent();                                                                
                                                                    
                    if (selectedNode.isRoot()){
                       JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ������.");                                       
                       
                       return;
                    }
                    
                    Category cat_old = (Category) selectedNode.getUserObject();                        
                    
                    if (cat_old.get_category_is_deleted()){
                       JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��������� ���������.");
                       
                       return;
                    }
                    
                    sd.setModal(true);
                    sd.setVisible(true); 
                    
                    if (sd.getResult()){
                        Category cat = sd.getCategory(cat_old.get_category_id());
                        client.service.updateCategory(cat);
                        refreshCat();
                    }
                    
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(GoodsDB.this, e.getMessage());
                }
                
              } else {
                    JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��������� � ������.");
              }
    }
 };
 t.start();
    
}

//�������� ���������
private void deleteCategory(){
    
                if (TreeCategory.hasEverChanged){
                    DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tc.curr_spath.getLastPathComponent();
                            if (selectedNode.isRoot()){
                                JOptionPane.showMessageDialog(GoodsDB.this, "���������� ������� ������.");                                       
                                return;
                            }
                            
                            JOptionPane optPane = new JOptionPane();                                   
                            UIManager.put("OptionPane.yesButtonText", "��");
                            UIManager.put("OptionPane.noButtonText", "���");
                            optPane.updateUI();
                            
                            Category cat = (Category) selectedNode.getUserObject();                                
                        
                        if (cat.get_category_is_deleted()){
                        
                            if (optPane.showConfirmDialog(GoodsDB.this,
                            "�������� �������� ���������?", "�������� ���������",
                            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                                cat.set_category_is_deleted(false);
                              try {
                            	  client.service.updateCategory(cat);
                              } catch (SQLException e){}
                            }
                        }
                        else {                                                                
                            if (optPane.showConfirmDialog(GoodsDB.this,
                                "�� ������������� ������ ������� ������ ���������?", "�������� ���������",
                                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){

                                try {                                                                   
                                   cat.set_category_is_deleted(true);
                                   client.service.updateCategory(cat);
                                } catch (SQLException e){
                                }
                                
                            }  else {
                                JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��������� � ������.");                        
                            }
                        }
            TreeCategory.hasEverChanged = false;
            refreshCat();
        }
}


private void moveToCategory(){
   if (tc.hasEverChanged) {
       try {
        MoveDialog md = new MoveDialog(GoodsDB.this, client.service.getUnits(true));
        md.setModal(true);
        md.setVisible(true);
       } catch (SQLException sqlex){
         System.out.println(sqlex);  
       }            
   } else {
        JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ��������� � �������");
   }
}

@Override public void actionPerformed(ActionEvent e) {
    if (e.getSource() instanceof Component) {
        Component c = (Component) e.getSource();                       
        
        if (c.getName().equals(INSERT_GD)) {
            insertGood();
        }
        
        if (c.getName().equals(CHANGE_GD)) {
            changeGood();
        }
        
        if (c.getName().equals(GIVE_GD)) {
            GoodTableModel stm = (GoodTableModel) goodsList.getModel();
            
            if (goodsList.getSelectedRow() >= 0) {
                GiveGoodDialog gd = new GiveGoodDialog(GoodsDB.this);                                
                gd.setModal(true);
                gd.setVisible(true);
                                   
                if (gd.getResult()){
                                      
                    Good s_good = stm.getGood(goodsList.getSelectedRow());
                    
                    int in_num = s_good.get_good_number();
                    if (gd.getNumber() == -1){ 
                        JOptionPane.showMessageDialog(GoodsDB.this, "�� �� ����� ���������� ��� ������."); 
                        return;                            
                    }
                    
                    if (in_num < gd.getNumber()){
                       JOptionPane.showMessageDialog(GoodsDB.this, "����� �� ������ ��������� ���������� �� ������."); 
                    } else {
                        try {
                        	client.service.giveGood(s_good, gd.getNumber());
                            reloadGoods();
                            
                        } catch (SQLException ex1){
                            System.out.println(ex1);
                        }
                    }
                }
                
          } else {
            JOptionPane.showMessageDialog(GoodsDB.this, "���������� �������� ����� � �������");
          }
        }
        
        if (c.getName().equals(DELETE_GD)) {
            deleteGood();
        }
        
        if (c.getName().equals(INSERT_UT)) {
            insertUnit();
        }
        
        if (c.getName().equals(CHANGE_UT)){
            changeUnit();
        }
        
        if (c.getName().equals(DELETE_UT)) {
            deleteUnit();
        }
        
        if (c.getName().equals(INSERT_CAT)){
            insertCategory();
        }
        
        if (c.getName().equals(CHANGE_CAT)){
            changeCategory();
        }
        
        if (c.getName().equals(DELETE_CAT)) {
            deleteCategory();
        }
        
        if (c.getName().equals(MOVE_CAT)) {
            moveToCategory();
        }
                                
        if (c.getName().equals(RECONNECT)){
                           
                reloadGoods();
                reloadUnits();
                refreshCat();                    
        }
        
        if (c.getName().equals(FILTER_GD)){
            try {
                FilterDialog fd = new FilterDialog(GoodsDB.this, GoodsDB.client.service.getCategories_leafs() );                                        
                
                fd.setModal(true);
                fd.setVisible(true);
                                    
                if (fd.getResult()){
                    int to_filter = fd.getCategory();
                                            
                    goodsList.setModel(new GoodTableModel(new Vector <Good> (client.service.getAllGoods(to_filter))));
                    d_width = setColumnsWidth(goodsList);
                    
                }
                
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(GoodsDB.this, ex.getMessage());
            }
        }
        
        if (c.getName().equals(ABOUT)){
            Aboutdialog.main();            
        }
        
        if (c.getName().equals(REMOVE_H)){
            reloadGoods();
        }
        
        if (c.getName().equals(HELP)){
            HelpDialog.main();
        }

    }
    
    if (e.getSource() == menuExit){
        System.exit(0);
    }
    
    if (e.getSource() == connOpts){
        OptionsDialog od = new OptionsDialog(this, host, port, db, login, password);
        od.setVisible(true);
        od.setModal(true);
        
        if (od.isSucceeded()){
            host = od.getHost();
            port = od.getPort();
            db = od.getDatabase();
            login = od.getUsername();
            password = od.getPassword();
        }            
    }
}
}
