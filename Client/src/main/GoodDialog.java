package main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.ListIterator;
import java.util.ArrayList;
import java.text.ParseException;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.util.Random;

import dblogic.Unit;
import dblogic.Category;
import dblogic.Good;

/*
 * @author Andrey Pervunin
 */

public final class GoodDialog extends JDialog implements ActionListener {
    
    private static final int D_HEIGHT = 300;   // ������ ����
    private final static int D_WIDTH = 500;   // ������ ����
    private final static int L_X = 10;      // ����� ������� ����� ��� ����
    private final static int L_W = 100;     // ������ ����� ��� ����
    private final static int C_W = 180;     // ������ ����
    // �������� ������ ���� - ������ ��� ������ ������� ��� ������
    private GoodsDB owner;
    // ��������� ������� ������
    private boolean result = false;
    // ��������� ������
    
    private JTextField goodName = new JTextField();
    
    private JFormattedTextField n_good = new JFormattedTextField(GetNumberFormat());
    private JFormattedTextField cost = new JFormattedTextField(GetNumberFormat2());

    private JTextField good_num = new JTextField(12);
    
    //private JComboBox unitList, categList;
    JComboBox category, meas_unit;
    
    public static MaskFormatter GetNumberFormat(){
        MaskFormatter mf = null;
        try {
          mf = new MaskFormatter("########");

          mf.setValidCharacters("0123456789");
        } catch (ParseException pe){
          System.err.println("Error in number format");
        }
        return mf;
      }
    
    //����� ��� �����
    public static MaskFormatter GetNumberFormat2(){
        MaskFormatter mf = null;
        try {
          mf = new MaskFormatter("######.##");
          mf.setValidCharacters("0123456789");
        } catch (ParseException pe){
          System.err.println("Error in number format");
        }
        return mf;
    }
    
    //����� ��� �����
    public static MaskFormatter GetNumberFormat3(){
        MaskFormatter mf = null;
        try {
          mf = new MaskFormatter("####");
          mf.setValidCharacters("0123456789");
        } catch (ParseException pe){
          System.err.println("Error in number format");
        }
        return mf;
    }
    
    List <Unit> filter_groups(List <Unit> in, int f){
        List <Unit> res = new ArrayList <Unit>();
        
        ListIterator lit = in.listIterator();
        Unit g;     int i = 0;    
        while (lit.hasNext()){
            if ((g = (Unit) lit.next()).get_unit_id() == f){
                res.add(g); i++;           
            }
        }  
        return res;
    }
            
    // ������ �������� �������� �������� - ���������� ������ ��� �����������
    @SuppressWarnings("static-access")
    public GoodDialog(final List<Unit> units,  List <Category> categories, boolean change, GoodsDB owner, Good std){
        // ����� ������� ������ ��� �������� ���� ��� ����������� ���������� ������
        // � ��� ����� ��� ���� ����� ������ � ����� ������ � ������� �����
        
        super();
        setResizable(false);
        
        this.owner = owner;
        
        good_num.addKeyListener(new java.awt.event.KeyListener() {

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }

            public void keyTyped(KeyEvent e) {
                char currentChar = e.getKeyChar();
                if (currentChar != 127 && currentChar != 8 && ((currentChar < '0') || (currentChar > '9'))) {
                    e.consume();
                    JOptionPane.showMessageDialog(null, "Invalid char: '" + currentChar + "'");
                }
            }
        });
        
        // ���������� ���������
        if (!change)
            setTitle("���������� ������ ������");
        else 
            setTitle("�������������� ������");
        
        getContentPane().setLayout(new FlowLayout());
        
        n_good.setValue((String) "00000000");
        cost.setValue((String) "000000.00");
                                
        getContentPane().setLayout(null);
                
        JLabel label;
        
        label = new JLabel("����� ������:", JLabel.RIGHT);
        label.setBounds(L_X, 5, L_W, 20);
        n_good.setBounds(L_X + L_W + 10, 7, C_W, 20);
        getContentPane().add(n_good);
        
        getContentPane().add(label);
        
        // ���
        label = new JLabel("������������: ", JLabel.RIGHT);
        label.setBounds(L_X, 32, L_W, 20);
        getContentPane().add(label);
        goodName.setBounds(L_X + L_W + 10, 32, C_W, 20);
        getContentPane().add(goodName);        
        
        label = new JLabel("����:", JLabel.RIGHT);
        label.setBounds(L_X, 55, L_W, 25);                
        getContentPane().add(label);
        
        label = new JLabel("���-�� ������:", JLabel.RIGHT);
        label.setBounds(L_X, 75, L_W, 25);
        
        getContentPane().add(label);
                        
        cost.setBounds(L_X + L_W + 10, 57, C_W / 2, 20);
        getContentPane().add(cost);        
        
        good_num.setBounds(L_X + L_W + 10, 80, C_W / 2, 20);
        getContentPane().add(good_num);
        
        label = new JLabel("������� �����.: ", JLabel.RIGHT);
        label.setBounds(L_X, 100, L_W, 25);
        getContentPane().add(label);
        
        try {            
            
        	category = new JComboBox(new Vector <Category> (GoodsDB.client.service.getCategories_leafs()));
            category.setBounds(L_X + L_W + 10, 130, C_W, 20);
            getContentPane().add(category);
            
            meas_unit = new JComboBox(new Vector <Unit> (GoodsDB.client.service.getUnits(true)));
            meas_unit.setBounds(L_X + L_W + 10, 105, C_W, 20);
            getContentPane().add(meas_unit);
            
        } catch (Exception ex){
            System.out.println("ex: "+ ex);
        }
                        
        final JButton btnAdd, rndNum;
        
        if (!change){
            btnAdd = new JButton("��������");
        } else {
            btnAdd = new JButton("��������");
        }
                                
        Action buttonsAction = new AbstractAction(){
            
            public void actionPerformed(ActionEvent e){
                try {                   
                    GoodDialog.this.actionPerformed(e);
                } catch (Exception ex){
                    System.out.println("Ex: "+ex);
                };                         
                setVisible(false);
            }
        };
        
        btnAdd.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "btn");
        btnAdd.getActionMap().put("btn", buttonsAction );
        
        btnAdd.setName("��������");
        
        btnAdd.addActionListener(this);
        btnAdd.setBounds(L_X + L_W + C_W + 10 + 50, 8, 100, 23);
        
        getContentPane().add(btnAdd);
        
        rndNum = new JButton(" ");
        rndNum.setBounds(L_X + L_W + C_W + 10, 7, 30, 19);
        rndNum.setName("random");
        rndNum.addActionListener(this);
        //rnd
        getContentPane().add(rndNum);
        
        
        JButton btnCancel = new JButton("�����");
        btnCancel.setName("�����");
        btnCancel.setBounds(L_X + L_W + C_W + 10 + 50, 40, 100, 25);
        
        btnCancel.getInputMap(JOptionPane.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "btn");
        btnCancel.getActionMap().put("btn", buttonsAction );
        btnCancel.addActionListener(this);
        
        getContentPane().add(btnCancel);        
        
        label = new JLabel("���������: ", JLabel.RIGHT);
        label.setBounds(L_X, 125, L_W, 25);
        getContentPane().add(label);
        
        // ������������� ��������� ����� ��� �������� - �� ��������� ������.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        // �������� ������� ������
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        // � ������ ������ �������� ��� �� ������, �������� ���������� �� ������ ���������� ����������
        
        if (change){
            
            n_good.setText(Integer.toString(std.get_good_id()));
            goodName.setText(std.get_good_name());
            
            String str = Double.toString(std.get_good_cost());
            
            int j = str.indexOf('.');
            while (str.subSequence(j, str.length()).length() < 3) str += "0";
            while (str.length() < 9) str = "0" + str;
            
            cost.setText(str);
            
            good_num.setText(Integer.toString(std.get_good_number()));
            
            try {
               category.setSelectedIndex(GoodsDB.client.service.getCategory_number(std.get_good_category())-1);               
               meas_unit.setSelectedIndex(GoodsDB.client.service.getUnit_number(std.get_good_unit())-1);
                    
            } catch (Exception se){
                System.out.println(se);
            }
        }
                
        setBounds(((int) d.getWidth() - GoodDialog.D_WIDTH) / 2, ((int) d.getHeight() - GoodDialog.D_HEIGHT) / 2,
                GoodDialog.D_WIDTH - 30, GoodDialog.D_HEIGHT - 100);
                
    }
  
    // ������� ������ � ���� ������ ������ � ���������������� ������ 
    public Good getGood() {
        Good good = new Good();
        
        Integer.parseInt(n_good.getText());
        good.set_good_id(Integer.parseInt(n_good.getText()));
        good.set_good_name(goodName.getText());
        good.set_good_cost(Double.parseDouble(cost.getText()));
        good.set_good_number(Integer.parseInt(good_num.getText()));                
        good.set_good_unit( ((Unit) meas_unit.getSelectedItem()).get_unit_id() );
        good.set_good_category( ((Category) category.getSelectedItem()).get_category_id());                        
        
        return good;
    }
   
    // �������� ���������, true - ������ ��, false - ������ Cancel
    public boolean getResult() {
        return result;
    }

    @Override public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        
        if (src.getName().equals("��������")) {
            
            if (good_num.getText().length() == 0) { 
                JOptionPane.showMessageDialog(GoodDialog.this, "�� �� ����� ���������� ������.");
                return;
            }
            
            if (goodName.getText().length() == 0){
                JOptionPane.showMessageDialog(GoodDialog.this, "�� �� ����� ������������ ������.");
                return;
            }
            
            result = true;
        }
        
        if (src.getName().equals("��������")) {
            
            result = true;
        }
        
        if (src.getName().equals("random")){
            Random rand = new Random();
            int i = rand.nextInt(100000000);
            String str = Integer.toString(rand.nextInt(100000000));
            
            while (str.length() < 8) str += "0";            
            n_good.setText(str);            
            
            return;
        }
        
        if (src.getName().equals("�����")){            
            result = false;
        }
        
        setVisible(false);
    }
}