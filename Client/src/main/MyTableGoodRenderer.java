package main;
import java.awt.Component;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.Color;
import dblogic.Unit;

/*
 * @author Andrey Pervunin
 */

/* 
 * переопределим рендерер
 */
/*class ColorRenderer extends JLabel implements TableCellRenderer
    {
     private String columnName;
     public ColorRenderer(String column)
         {
         this.columnName = column;
         setOpaque(true);
     }
     
     public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column)
         {
         Object columnValue=table.getValueAt(row,table.getColumnModel().getColumnIndex(columnName));
        
         if (value != null) setText(value.toString());
         if(isSelected)
             {
             setBackground(table.getSelectionBackground());
             setForeground(table.getSelectionForeground());
         }
         else
             {
             setBackground(table.getBackground());
             setForeground(table.getForeground());
             if (columnValue.equals("1")) setBackground(java.awt.Color.pink);
             if (columnValue.equals("2")) setBackground(java.awt.Color.green);
             if (columnValue.equals("3")) setBackground(java.awt.Color.red);
             if (columnValue.equals("4")) setBackground(java.awt.Color.blue);
            
         }
         return this;
     }
}*/
/*
DefaultTableCellRenderer jTableRenderer = new DefaultTableCellRenderer() {
                    @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    
                    try {
                        //System.out.println(row);
                        Unit u = dbconnection.getNthUnit(row);
                        
                        //System.out.println(row + " " + u.get_unit_id());
                        
                        if (u.get_unit_is_deleted())
                            cell.setBackground(Color.RED);
                        else 
                            cell.setBackground(Color.WHITE);
                        
                    } catch (SQLException e){
                        System.out.println(e);
                    }
                    
                    //if (!isSelected){                        
                        //cell.setBackground(row % 2 == 1 ? Color.WHITE : Color.GRAY);
                    //}
                    
                  return cell;
                 }
                };   
*/
/*
DefaultTableCellRenderer jTableRenderer = new DefaultTableCellRenderer() {
02	    @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
03	        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
04	        if (!isSelected) {
05	            cell.setBackground(row % 2 == 1 ? Color.WHITE : Color.GRAY);
06	        }
07	        return cell;
08	    }
09	};
*/

public class MyTableGoodRenderer extends DefaultTableCellRenderer {
     public MyTableGoodRenderer(){
         super();
     }
 
    
    @Override public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,  
             boolean hasFocus, int row, int column){  
         
        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
         boolean diff_color = ((MyTableModel) table.getModel()).getHighlighted(row);
         setHorizontalAlignment(SwingConstants.CENTER);
         
         float a[] = new float[3];
         Color.RGBtoHSB(184, 207, 229, a);
                           
         if (diff_color)
            {
                if (isSelected)
                    {                      
                        setBackground(Color.getHSBColor(a[0],a[1],a[2]));
                        setForeground(Color.BLACK);
                    } else {
                        setBackground(Color.YELLOW);
                        setForeground(Color.BLACK);
                    }
            } else 
            {
               
               if (isSelected)
                    {                      
                        setBackground(Color.getHSBColor(a[0],a[1],a[2]));
                        setForeground(Color.BLACK);
                    } else {

                        setForeground(Color.BLUE);
                        setForeground(Color.BLACK);
                    }

            }        
         return this;
    }   
}
     
  