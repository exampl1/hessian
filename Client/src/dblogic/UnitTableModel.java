package dblogic;

/*
 * @author Andrey Pervunin
 */

import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import main.MyTableModel;

public class UnitTableModel extends AbstractTableModel implements MyTableModel {
    private Vector units;
     
    public UnitTableModel(Vector units){
        super();
        this.units = units;
    }
    
    @Override public int getRowCount() {
        if (units != null) {
            return units.size();
        }
        
        return 0;
    }    
    
    @Override public int getColumnCount() {
        return 3;
    }
    
    @Override public String getColumnName(int column) {
        //String[] colNames = {"Номер", "Краткое наименование", "Полное наименование"};
    	String[] colNames = {"�����", "������� ������������", "������ ������������"};
    	return colNames[column];
    }
    
    @Override public Object getValueAt(int rowIndex, int columnIndex){      
        if (units != null){
            // Получаем из вектора единицу измерения
            Unit unit = (Unit) units.get(rowIndex);  
            try {
                switch (columnIndex){
                    case 0:                        
                        return unit.get_unit_id(); 
                    case 1:                        
                        return unit.get_short_name();
                    case 2:
                        return unit.get_full_name();
                }    
            }catch (Exception e){}
        }
        return null;
    }
    
   public Unit getUnit(int rowIndex) {
        if (units != null) {
            if (rowIndex < units.size() && rowIndex >= 0) {
                return (Unit) units.get(rowIndex);
            }
        }
        return null;
   }
   
   @Override public boolean getHighlighted(int rowIndex){
        return false;
   }
}
