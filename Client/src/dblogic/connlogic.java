package dblogic;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import com.devx.hessian.client.*;

/*
 * @author Andrey Pervunin
 */

/*
 *  ������ ������ � ����� ������, ��������� � �. �.
 */

public class connlogic {
    private static Connection conn;
    private static connlogic instance;
    MainClient client;
    
    // ����������� ������ ������ � ����� ������
    public connlogic(String host, String port, String db, String login, String password) throws Exception {
        	String url = "jdbc:h2:~/"+db;
        	ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
			client = (MainClient) context.getBean("mainClient");
            
    }
     
    public static synchronized connlogic getInstance(String _host, String _port, String _db, String _login, String _password) throws Exception {
        if (instance == null) {
            instance = new connlogic(_host, _port, _db, _login, _password);
        }
        
        return instance;
    }
     
}
