package dblogic;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.Serializable;

/*
 * @author Andrey Pervunin
 */

public final class Good implements Serializable {

    private int good_id;
    private String good_name;
    private double good_cost;
    private int qty;
    private int unit;
    private int category;
    private boolean is_deleted;
    
    public Good() {
    
    }
    
    public Good(int good_id, String name, int good_cost, int qty, int unit, int category, boolean is_deleted){
    	super();
    	set_good_id(good_id);
    	set_good_name(name);
    	set_good_cost(good_cost);
    	set_good_number(qty);
    	set_good_unit(unit);
    	set_good_category(category);
    	set_good_is_deleted(is_deleted);
    }
    
    public boolean equals(Object o){
        if (o == this) 
            return true;
        
        if (!(o instanceof Good))
            return false;
        
        Good g = (Good) o;
        
        return (g.good_id == good_id) &&
                (g.good_name == good_name) &&
                (g.is_deleted == is_deleted) &&
                (g.category == category) &&
                (g.good_cost == good_cost) &&
                (g.qty == qty) &&
                (g.unit == unit);
    }
    
    public int hashCode(){
        int result=17;
        result = 37*result + good_id;
        result = 37*result + good_name.hashCode();
        result = 49*result + (int) (100*good_cost);
        result = 49*result + (int) 3*qty;
        result = 49*result + (int) 7*unit;
        result += category;
        if (is_deleted) result +=3;
        return result;
    }
    
    public Good (ResultSet rs) throws SQLException {        
        
        set_good_id(rs.getInt(1));
        set_good_name(rs.getString(2));
        set_good_cost(rs.getDouble(3));
        set_good_unit(rs.getInt(4));
        set_good_number(rs.getInt(5));
        set_good_category(rs.getInt(6));
        
    }
    
    public int get_good_id() {
        return good_id;
    }
    
    public void set_good_id(int _good_id) {
        this.good_id = _good_id;
    }
    
    public String get_good_name() {
        return good_name;
    }
    
    public void set_good_name(String _good_name) {
        this.good_name = _good_name;
    }
    
    public double get_good_cost() {
       return good_cost;
    }
    
    public void set_good_cost(double _good_cost){
       good_cost = _good_cost;
    }
    
    public int get_good_number() {
       return qty;
    }
    
    public void set_good_number(int _qty) {
       qty = _qty;
    }
    
    public int get_good_unit(){
       return unit;
    }
    
    public void set_good_unit(int _good_unit){
       unit = _good_unit;
    }
    
    public int get_good_category() {
       return category;
    }
    
    public void set_good_category(int _categ) {
       category = _categ;
    }
    
    public void set_good_is_deleted(boolean _flag) {
       is_deleted = _flag;
    }
    
    public boolean get_good_is_deleted(){
       return is_deleted;
    }
}